﻿/**
  ******************************************************************************
  * @date    3-Sep-2017
  * @version V1.0.3
  * @author  Vasiliev Sergey
  * @brief   Консольный Терминал - ЗАПРОС/ОТВЕТ
  ******************************************************************************
*/
using System;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;

using l420;
using micro.sdk;

public partial class main
{
    const string KEY_BANNER     = "-b";
    const string KEY_HELP       = "-?";
    const string KEY_PORT       = "--port";         // ttyUSB0:115200:8N1:42:42
    const string KEY_PORT_LIST  = "--port-list";
    const string KEY_DEBUG      = "--debug";
    const string KEY_LOOP       = "--loop";
    const string KEY_HEX        = "--hex";
    const string KEY_FILE       = "--file";
    const string KEY_GUI        = "--gui";              // консоль + окно


    static Parity GetParityFrom(char _key)
    {
        switch (_key)
        {
            case 'N': return Parity.None;
            case 'E': return Parity.Even;

        }
        return Parity.None;
    }

    static StopBits GetStopBit(char _key)
    {

        switch (_key)
        {
            case '0': return StopBits.None;
            case '1': return StopBits.One;
            case '2': return StopBits.Two;
            case '3': return StopBits.OnePointFive;

        }
        return StopBits.None;
    }

    static int GetDataBit(char _key)
    {
        int number;

        bool success = Int32.TryParse(_key.ToString(), out number);
        if (success)
        {

            return number;
        }


        return 8;
    }

    [STAThread]
    static void Main()
    {
        log.begin( ConsoleColor.White );
        if( argv.check_key(KEY_BANNER)  || argv.count == 0) log.print_random_banner();
        if( argv.check_key(KEY_HELP)    || argv.count == 0) log.print( _help );
        log.end();
        
   
        if(argv.check_key( KEY_PORT_LIST ))
        {
            log.print( intercom.usart_EnumPort() );
        }
        
        if(argv.check_key( KEY_PORT ))
        {
            intercom.use_debug         = argv.check_key( KEY_DEBUG );

            //intercom.eventPinChangned  += delegate(){log.print("IRQ_PinChanged");};
            //intercom.eventIRQError     += delegate(){log.print("IRQ_Error");};
            //intercom.eventPortNotFound += delegate(){log.print("PortNotFound");};
            //intercom.eventConnect      += delegate(){};
            intercom.eventDataIncoming += delegate () { };


            var port_arg = argv.get_str(KEY_PORT, _bydefault: "auto");
            var port_arg_buff = port_arg.Split(':'); //COM5:115200:8N1:42:42

            // валидация выхода за границу массива аргументов
            // последний ключ без значения
            intercom.usart_Open( port_arg_buff[0] );

            if (intercom._serial == null)
            {
                log.print("отсутствует серийный порт");
            }
            else
            {

                intercom._serial.BaudRate = (1 >= port_arg_buff.Length) ? 115200 : int.Parse(port_arg_buff[1]);

                var _buff = (2 >= port_arg_buff.Length) ? new char[0] : port_arg_buff[2].ToCharArray();
                intercom._serial.DataBits = (0 >= _buff.Length) ? 8 : GetDataBit(_buff[0]);
                intercom._serial.Parity = (1 >= _buff.Length) ? Parity.None : GetParityFrom(_buff[1]);
                intercom._serial.StopBits = (2 >= _buff.Length) ? StopBits.One : GetStopBit(_buff[2]);

                intercom._serial.Open();
            }
        }

        if (argv.check_key(KEY_GUI))
        { 
            switch(argv.get_num(KEY_GUI, _bydefault:0))
            {
                case 1: Application.Run(new app.winform_NRF());    break;
                case 2: Application.Run(new app.winform_SPI());    break;
                case 3: Application.Run(new app.winform_USART());  break;

                case 4: Application.Run(new app.winform_R3C0NF1GUR4T0R()); break;
                case 5: Application.Run(new app.winform_LL_EBD()); break;

                case 6: Application.Run(new app.winform_LL_MOTO_PIANO()); break;
                case 7: Application.Run(new MassMeter.massmeter_form()); break;
                case 8: Application.Run(new Voltmeter.voltmeter()); break;
                case 9: Application.Run(new Generator.winform_LL_Generator()); break;
                default: log.print("Nothing to do"); break;
            }
        }
        else if(argv.check_key( KEY_LOOP ))
        {
            while(true)
            {
                Thread.Sleep( argv.get_num( KEY_LOOP, _bydefault:150 ) );
                intercom.usart_TX = intercom._parse( argv.get_str( KEY_HEX ) );
            }
        }
        else
        {
            intercom.usart_TX = intercom._parse( argv.get_str( KEY_HEX ) );
        }
        
        Thread.Sleep( 250 );
        
        if(argv.check_key( KEY_DEBUG ))
        {
            intercom.usart_DumpLast(ref intercom.usart_RX);
        }
    }

const string _help =
@"
   -? show this message
   -b show banner
   --debug
   --gui  |*0|1|2|3|
   --port-list
   --port com[1..9] | *auto | COM5:115200:8N1
   --hex  00-00-00-00-00
   --file barz.txt
   --loop 150

    usage: 
   $intercom --port com3 --hex MEMREAD-@40013800-22 --debug --loop 500 -?
";
}//class end
