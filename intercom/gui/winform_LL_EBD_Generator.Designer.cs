﻿namespace app
{
    partial class winform_LL_EBD
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_baudrate = new System.Windows.Forms.ComboBox();
            this.label_RX = new System.Windows.Forms.Label();
            this.label_TX = new System.Windows.Forms.Label();
            this.comboBox_ports = new System.Windows.Forms.ComboBox();
            this.open_port = new System.Windows.Forms.Button();
            this.label_port_status = new System.Windows.Forms.Label();
            this.close_port = new System.Windows.Forms.Button();
            this.button_reset = new System.Windows.Forms.Button();
            this.numericUpDown_freq = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_duration = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_scale = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox_SP = new System.Windows.Forms.GroupBox();
            this.label_dir = new System.Windows.Forms.Label();
            this.label_scale_1 = new System.Windows.Forms.Label();
            this.label_Pulse_10kHz = new System.Windows.Forms.Label();
            this.label_rabbit = new System.Windows.Forms.Label();
            this.label_duration_50 = new System.Windows.Forms.Label();
            this.label_duration_100 = new System.Windows.Forms.Label();
            this.label_duration_75 = new System.Windows.Forms.Label();
            this.label_duration_25 = new System.Windows.Forms.Label();
            this.label_duration_0 = new System.Windows.Forms.Label();
            this.label_Pulse_10MHz = new System.Windows.Forms.Label();
            this.label_Pulse_440Hz = new System.Windows.Forms.Label();
            this.label_pulse_1Hz = new System.Windows.Forms.Label();
            this.label_Pulse_1MHz = new System.Windows.Forms.Label();
            this.label_pulse_50Hz = new System.Windows.Forms.Label();
            this.label_Pulse_1kHz = new System.Windows.Forms.Label();
            this.label_MRC = new System.Windows.Forms.Label();
            this.button_scale_minus = new System.Windows.Forms.Button();
            this.button_scale_plus = new System.Windows.Forms.Button();
            this.label_scale_calc = new System.Windows.Forms.Label();
            this.button_duration_minus = new System.Windows.Forms.Button();
            this.button_duration_plus = new System.Windows.Forms.Button();
            this.label_duration_calc = new System.Windows.Forms.Label();
            this.button_freq_minus = new System.Windows.Forms.Button();
            this.button_freq_plus = new System.Windows.Forms.Button();
            this.label_freq_calc = new System.Windows.Forms.Label();
            this.button_set_default = new System.Windows.Forms.Button();
            this.groupBox_MRC = new System.Windows.Forms.GroupBox();
            this.label_duty_cycle = new System.Windows.Forms.Label();
            this.label_duty_cycle_l = new System.Windows.Forms.Label();
            this.label_pause = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown_ccr2 = new System.Windows.Forms.NumericUpDown();
            this.label_period = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown_ccr1 = new System.Windows.Forms.NumericUpDown();
            this.label_pulse = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numericUpDown_psc = new System.Windows.Forms.NumericUpDown();
            this.label_gen_clock = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_arr = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_freq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_duration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_scale)).BeginInit();
            this.groupBox_SP.SuspendLayout();
            this.groupBox_MRC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ccr2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ccr1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_psc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_arr)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox_baudrate);
            this.groupBox1.Controls.Add(this.label_RX);
            this.groupBox1.Controls.Add(this.label_TX);
            this.groupBox1.Controls.Add(this.comboBox_ports);
            this.groupBox1.Controls.Add(this.open_port);
            this.groupBox1.Controls.Add(this.label_port_status);
            this.groupBox1.Controls.Add(this.close_port);
            this.groupBox1.Location = new System.Drawing.Point(12, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(502, 51);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection";
            // 
            // comboBox_baudrate
            // 
            this.comboBox_baudrate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.comboBox_baudrate.ForeColor = System.Drawing.Color.Orange;
            this.comboBox_baudrate.FormattingEnabled = true;
            this.comboBox_baudrate.Location = new System.Drawing.Point(87, 19);
            this.comboBox_baudrate.Name = "comboBox_baudrate";
            this.comboBox_baudrate.Size = new System.Drawing.Size(74, 21);
            this.comboBox_baudrate.TabIndex = 8;
            // 
            // label_RX
            // 
            this.label_RX.AutoSize = true;
            this.label_RX.Location = new System.Drawing.Point(428, 24);
            this.label_RX.Name = "label_RX";
            this.label_RX.Size = new System.Drawing.Size(22, 13);
            this.label_RX.TabIndex = 7;
            this.label_RX.Text = "RX";
            this.label_RX.Click += new System.EventHandler(this.label_RX_Click);
            // 
            // label_TX
            // 
            this.label_TX.AutoSize = true;
            this.label_TX.Location = new System.Drawing.Point(456, 24);
            this.label_TX.Name = "label_TX";
            this.label_TX.Size = new System.Drawing.Size(21, 13);
            this.label_TX.TabIndex = 6;
            this.label_TX.Text = "TX";
            // 
            // comboBox_ports
            // 
            this.comboBox_ports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.comboBox_ports.ForeColor = System.Drawing.Color.Orange;
            this.comboBox_ports.FormattingEnabled = true;
            this.comboBox_ports.Location = new System.Drawing.Point(6, 19);
            this.comboBox_ports.Name = "comboBox_ports";
            this.comboBox_ports.Size = new System.Drawing.Size(75, 21);
            this.comboBox_ports.TabIndex = 4;
            // 
            // open_port
            // 
            this.open_port.ForeColor = System.Drawing.Color.Orange;
            this.open_port.Location = new System.Drawing.Point(167, 19);
            this.open_port.Name = "open_port";
            this.open_port.Size = new System.Drawing.Size(75, 23);
            this.open_port.TabIndex = 0;
            this.open_port.Text = "Open";
            this.open_port.UseVisualStyleBackColor = true;
            this.open_port.Click += new System.EventHandler(this.open_port_Click);
            // 
            // label_port_status
            // 
            this.label_port_status.AutoSize = true;
            this.label_port_status.ForeColor = System.Drawing.Color.OrangeRed;
            this.label_port_status.Location = new System.Drawing.Point(342, 24);
            this.label_port_status.Name = "label_port_status";
            this.label_port_status.Size = new System.Drawing.Size(62, 13);
            this.label_port_status.TabIndex = 3;
            this.label_port_status.Text = "Don`t panic";
            this.label_port_status.Click += new System.EventHandler(this.label_port_status_Click);
            // 
            // close_port
            // 
            this.close_port.ForeColor = System.Drawing.Color.Orange;
            this.close_port.Location = new System.Drawing.Point(248, 19);
            this.close_port.Name = "close_port";
            this.close_port.Size = new System.Drawing.Size(75, 23);
            this.close_port.TabIndex = 1;
            this.close_port.Text = "Close";
            this.close_port.UseVisualStyleBackColor = true;
            this.close_port.Click += new System.EventHandler(this.close_port_Click);
            // 
            // button_reset
            // 
            this.button_reset.BackColor = System.Drawing.Color.Red;
            this.button_reset.Font = new System.Drawing.Font("Lucida Console", 7.25F);
            this.button_reset.Location = new System.Drawing.Point(143, 14);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(22, 20);
            this.button_reset.TabIndex = 9;
            this.button_reset.Text = "0\r\n";
            this.button_reset.UseVisualStyleBackColor = false;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // numericUpDown_freq
            // 
            this.numericUpDown_freq.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_freq.Location = new System.Drawing.Point(412, 126);
            this.numericUpDown_freq.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDown_freq.Name = "numericUpDown_freq";
            this.numericUpDown_freq.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_freq.TabIndex = 10;
            this.numericUpDown_freq.Visible = false;
            this.numericUpDown_freq.ValueChanged += new System.EventHandler(this.numericUpDown_freq_ValueChanged);
            // 
            // numericUpDown_duration
            // 
            this.numericUpDown_duration.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_duration.Location = new System.Drawing.Point(156, 126);
            this.numericUpDown_duration.Name = "numericUpDown_duration";
            this.numericUpDown_duration.Size = new System.Drawing.Size(45, 20);
            this.numericUpDown_duration.TabIndex = 11;
            this.numericUpDown_duration.Visible = false;
            this.numericUpDown_duration.ValueChanged += new System.EventHandler(this.numericUpDown_duration_ValueChanged);
            // 
            // numericUpDown_scale
            // 
            this.numericUpDown_scale.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_scale.Location = new System.Drawing.Point(310, 126);
            this.numericUpDown_scale.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.numericUpDown_scale.Name = "numericUpDown_scale";
            this.numericUpDown_scale.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_scale.TabIndex = 12;
            this.numericUpDown_scale.Visible = false;
            this.numericUpDown_scale.ValueChanged += new System.EventHandler(this.numericUpDown_scale_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.DarkOrange;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 16);
            this.label1.TabIndex = 9;
            this.label1.Text = "Частота, Hz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.DarkOrange;
            this.label2.Location = new System.Drawing.Point(212, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Множитель";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.DarkOrange;
            this.label3.Location = new System.Drawing.Point(7, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 16);
            this.label3.TabIndex = 14;
            this.label3.Text = "Заполнение, %";
            // 
            // groupBox_SP
            // 
            this.groupBox_SP.Controls.Add(this.label_dir);
            this.groupBox_SP.Controls.Add(this.label_scale_1);
            this.groupBox_SP.Controls.Add(this.label_Pulse_10kHz);
            this.groupBox_SP.Controls.Add(this.label_rabbit);
            this.groupBox_SP.Controls.Add(this.label_duration_50);
            this.groupBox_SP.Controls.Add(this.label_duration_100);
            this.groupBox_SP.Controls.Add(this.label_duration_75);
            this.groupBox_SP.Controls.Add(this.label_duration_25);
            this.groupBox_SP.Controls.Add(this.label_duration_0);
            this.groupBox_SP.Controls.Add(this.label_Pulse_10MHz);
            this.groupBox_SP.Controls.Add(this.label_Pulse_440Hz);
            this.groupBox_SP.Controls.Add(this.label_pulse_1Hz);
            this.groupBox_SP.Controls.Add(this.label_Pulse_1MHz);
            this.groupBox_SP.Controls.Add(this.label_pulse_50Hz);
            this.groupBox_SP.Controls.Add(this.label_Pulse_1kHz);
            this.groupBox_SP.Controls.Add(this.label_MRC);
            this.groupBox_SP.Controls.Add(this.button_scale_minus);
            this.groupBox_SP.Controls.Add(this.button_scale_plus);
            this.groupBox_SP.Controls.Add(this.numericUpDown_scale);
            this.groupBox_SP.Controls.Add(this.label_scale_calc);
            this.groupBox_SP.Controls.Add(this.numericUpDown_duration);
            this.groupBox_SP.Controls.Add(this.button_duration_minus);
            this.groupBox_SP.Controls.Add(this.button_duration_plus);
            this.groupBox_SP.Controls.Add(this.label_duration_calc);
            this.groupBox_SP.Controls.Add(this.button_freq_minus);
            this.groupBox_SP.Controls.Add(this.button_freq_plus);
            this.groupBox_SP.Controls.Add(this.label_freq_calc);
            this.groupBox_SP.Controls.Add(this.button_set_default);
            this.groupBox_SP.Controls.Add(this.label1);
            this.groupBox_SP.Controls.Add(this.label3);
            this.groupBox_SP.Controls.Add(this.button_reset);
            this.groupBox_SP.Controls.Add(this.label2);
            this.groupBox_SP.Controls.Add(this.numericUpDown_freq);
            this.groupBox_SP.Location = new System.Drawing.Point(12, 56);
            this.groupBox_SP.Name = "groupBox_SP";
            this.groupBox_SP.Size = new System.Drawing.Size(502, 231);
            this.groupBox_SP.TabIndex = 15;
            this.groupBox_SP.TabStop = false;
            this.groupBox_SP.Text = "Генератор прямоугольных импульсов";
            this.groupBox_SP.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label_dir
            // 
            this.label_dir.AutoSize = true;
            this.label_dir.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_dir.ForeColor = System.Drawing.Color.Black;
            this.label_dir.Location = new System.Drawing.Point(473, 32);
            this.label_dir.Name = "label_dir";
            this.label_dir.Size = new System.Drawing.Size(25, 31);
            this.label_dir.TabIndex = 39;
            this.label_dir.Text = "*";
            this.label_dir.Click += new System.EventHandler(this.label_dir_Click);
            // 
            // label_scale_1
            // 
            this.label_scale_1.AutoSize = true;
            this.label_scale_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_scale_1.ForeColor = System.Drawing.Color.SandyBrown;
            this.label_scale_1.Location = new System.Drawing.Point(275, 206);
            this.label_scale_1.Name = "label_scale_1";
            this.label_scale_1.Size = new System.Drawing.Size(16, 17);
            this.label_scale_1.TabIndex = 38;
            this.label_scale_1.Text = "1";
            this.label_scale_1.Click += new System.EventHandler(this.label_scale_1_Click);
            // 
            // label_Pulse_10kHz
            // 
            this.label_Pulse_10kHz.AutoSize = true;
            this.label_Pulse_10kHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Pulse_10kHz.ForeColor = System.Drawing.Color.Chocolate;
            this.label_Pulse_10kHz.Location = new System.Drawing.Point(312, 15);
            this.label_Pulse_10kHz.Name = "label_Pulse_10kHz";
            this.label_Pulse_10kHz.Size = new System.Drawing.Size(48, 17);
            this.label_Pulse_10kHz.TabIndex = 37;
            this.label_Pulse_10kHz.Text = "10kHz";
            this.label_Pulse_10kHz.Click += new System.EventHandler(this.label_Pulse_10kHz_Click);
            // 
            // label_rabbit
            // 
            this.label_rabbit.AutoSize = true;
            this.label_rabbit.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_rabbit.ForeColor = System.Drawing.Color.Black;
            this.label_rabbit.Location = new System.Drawing.Point(455, 13);
            this.label_rabbit.Name = "label_rabbit";
            this.label_rabbit.Size = new System.Drawing.Size(25, 31);
            this.label_rabbit.TabIndex = 36;
            this.label_rabbit.Text = "*";
            this.label_rabbit.Click += new System.EventHandler(this.label_rabbit_Click);
            // 
            // label_duration_50
            // 
            this.label_duration_50.AutoSize = true;
            this.label_duration_50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_duration_50.ForeColor = System.Drawing.Color.Chocolate;
            this.label_duration_50.Location = new System.Drawing.Point(112, 207);
            this.label_duration_50.Name = "label_duration_50";
            this.label_duration_50.Size = new System.Drawing.Size(24, 17);
            this.label_duration_50.TabIndex = 35;
            this.label_duration_50.Text = "50";
            this.label_duration_50.Click += new System.EventHandler(this.label_duration_50_Click);
            // 
            // label_duration_100
            // 
            this.label_duration_100.AutoSize = true;
            this.label_duration_100.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_duration_100.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_duration_100.Location = new System.Drawing.Point(157, 207);
            this.label_duration_100.Name = "label_duration_100";
            this.label_duration_100.Size = new System.Drawing.Size(44, 17);
            this.label_duration_100.TabIndex = 34;
            this.label_duration_100.Text = "100%";
            this.label_duration_100.Click += new System.EventHandler(this.label_duration_100_Click);
            // 
            // label_duration_75
            // 
            this.label_duration_75.AutoSize = true;
            this.label_duration_75.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_duration_75.ForeColor = System.Drawing.Color.Chocolate;
            this.label_duration_75.Location = new System.Drawing.Point(136, 207);
            this.label_duration_75.Name = "label_duration_75";
            this.label_duration_75.Size = new System.Drawing.Size(24, 17);
            this.label_duration_75.TabIndex = 33;
            this.label_duration_75.Text = "75";
            this.label_duration_75.Click += new System.EventHandler(this.label_duration_75_Click);
            // 
            // label_duration_25
            // 
            this.label_duration_25.AutoSize = true;
            this.label_duration_25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_duration_25.ForeColor = System.Drawing.Color.Chocolate;
            this.label_duration_25.Location = new System.Drawing.Point(88, 207);
            this.label_duration_25.Name = "label_duration_25";
            this.label_duration_25.Size = new System.Drawing.Size(24, 17);
            this.label_duration_25.TabIndex = 32;
            this.label_duration_25.Text = "25";
            this.label_duration_25.Click += new System.EventHandler(this.label_duration_25_Click);
            // 
            // label_duration_0
            // 
            this.label_duration_0.AutoSize = true;
            this.label_duration_0.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_duration_0.ForeColor = System.Drawing.Color.SandyBrown;
            this.label_duration_0.Location = new System.Drawing.Point(70, 207);
            this.label_duration_0.Name = "label_duration_0";
            this.label_duration_0.Size = new System.Drawing.Size(16, 17);
            this.label_duration_0.TabIndex = 31;
            this.label_duration_0.Text = "0";
            this.label_duration_0.Click += new System.EventHandler(this.label_duration_0_Click);
            // 
            // label_Pulse_10MHz
            // 
            this.label_Pulse_10MHz.AutoSize = true;
            this.label_Pulse_10MHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Pulse_10MHz.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_Pulse_10MHz.Location = new System.Drawing.Point(402, 15);
            this.label_Pulse_10MHz.Name = "label_Pulse_10MHz";
            this.label_Pulse_10MHz.Size = new System.Drawing.Size(52, 17);
            this.label_Pulse_10MHz.TabIndex = 30;
            this.label_Pulse_10MHz.Text = "10MHz";
            this.label_Pulse_10MHz.Click += new System.EventHandler(this.label_Pulse_10MHz_Click);
            // 
            // label_Pulse_440Hz
            // 
            this.label_Pulse_440Hz.AutoSize = true;
            this.label_Pulse_440Hz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Pulse_440Hz.ForeColor = System.Drawing.Color.Chocolate;
            this.label_Pulse_440Hz.Location = new System.Drawing.Point(232, 15);
            this.label_Pulse_440Hz.Name = "label_Pulse_440Hz";
            this.label_Pulse_440Hz.Size = new System.Drawing.Size(49, 17);
            this.label_Pulse_440Hz.TabIndex = 29;
            this.label_Pulse_440Hz.Text = "440Hz";
            this.label_Pulse_440Hz.Click += new System.EventHandler(this.label_Pulse_440Hz_Click);
            // 
            // label_pulse_1Hz
            // 
            this.label_pulse_1Hz.AutoSize = true;
            this.label_pulse_1Hz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pulse_1Hz.ForeColor = System.Drawing.Color.SandyBrown;
            this.label_pulse_1Hz.Location = new System.Drawing.Point(164, 15);
            this.label_pulse_1Hz.Name = "label_pulse_1Hz";
            this.label_pulse_1Hz.Size = new System.Drawing.Size(33, 17);
            this.label_pulse_1Hz.TabIndex = 28;
            this.label_pulse_1Hz.Text = "1Hz";
            this.label_pulse_1Hz.Click += new System.EventHandler(this.label_pulse_1Hz_Click);
            // 
            // label_Pulse_1MHz
            // 
            this.label_Pulse_1MHz.AutoSize = true;
            this.label_Pulse_1MHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Pulse_1MHz.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_Pulse_1MHz.Location = new System.Drawing.Point(360, 15);
            this.label_Pulse_1MHz.Name = "label_Pulse_1MHz";
            this.label_Pulse_1MHz.Size = new System.Drawing.Size(44, 17);
            this.label_Pulse_1MHz.TabIndex = 27;
            this.label_Pulse_1MHz.Text = "1MHz";
            this.label_Pulse_1MHz.Click += new System.EventHandler(this.label_Pulse_1MHz_Click);
            // 
            // label_pulse_50Hz
            // 
            this.label_pulse_50Hz.AutoSize = true;
            this.label_pulse_50Hz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pulse_50Hz.ForeColor = System.Drawing.Color.SandyBrown;
            this.label_pulse_50Hz.Location = new System.Drawing.Point(195, 15);
            this.label_pulse_50Hz.Name = "label_pulse_50Hz";
            this.label_pulse_50Hz.Size = new System.Drawing.Size(41, 17);
            this.label_pulse_50Hz.TabIndex = 26;
            this.label_pulse_50Hz.Text = "50Hz";
            this.label_pulse_50Hz.Click += new System.EventHandler(this.label_pulse_50Hz_Click);
            // 
            // label_Pulse_1kHz
            // 
            this.label_Pulse_1kHz.AutoSize = true;
            this.label_Pulse_1kHz.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_Pulse_1kHz.ForeColor = System.Drawing.Color.Chocolate;
            this.label_Pulse_1kHz.Location = new System.Drawing.Point(275, 15);
            this.label_Pulse_1kHz.Name = "label_Pulse_1kHz";
            this.label_Pulse_1kHz.Size = new System.Drawing.Size(40, 17);
            this.label_Pulse_1kHz.TabIndex = 25;
            this.label_Pulse_1kHz.Text = "1kHz";
            this.label_Pulse_1kHz.Click += new System.EventHandler(this.label_Pulse_1kHz_Click);
            // 
            // label_MRC
            // 
            this.label_MRC.AutoSize = true;
            this.label_MRC.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_MRC.ForeColor = System.Drawing.Color.Black;
            this.label_MRC.Location = new System.Drawing.Point(474, 13);
            this.label_MRC.Name = "label_MRC";
            this.label_MRC.Size = new System.Drawing.Size(25, 31);
            this.label_MRC.TabIndex = 9;
            this.label_MRC.Text = "*";
            this.label_MRC.Click += new System.EventHandler(this.label_MRC_Click);
            // 
            // button_scale_minus
            // 
            this.button_scale_minus.ForeColor = System.Drawing.Color.Orange;
            this.button_scale_minus.Location = new System.Drawing.Point(215, 188);
            this.button_scale_minus.Name = "button_scale_minus";
            this.button_scale_minus.Size = new System.Drawing.Size(50, 30);
            this.button_scale_minus.TabIndex = 24;
            this.button_scale_minus.Text = "-";
            this.button_scale_minus.UseVisualStyleBackColor = true;
            this.button_scale_minus.Click += new System.EventHandler(this.button_scale_minus_Click);
            // 
            // button_scale_plus
            // 
            this.button_scale_plus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.button_scale_plus.ForeColor = System.Drawing.Color.Orange;
            this.button_scale_plus.Location = new System.Drawing.Point(215, 152);
            this.button_scale_plus.Name = "button_scale_plus";
            this.button_scale_plus.Size = new System.Drawing.Size(50, 30);
            this.button_scale_plus.TabIndex = 23;
            this.button_scale_plus.Text = "+";
            this.button_scale_plus.UseVisualStyleBackColor = false;
            this.button_scale_plus.Click += new System.EventHandler(this.button_scale_plus_Click);
            // 
            // label_scale_calc
            // 
            this.label_scale_calc.AutoSize = true;
            this.label_scale_calc.Font = new System.Drawing.Font("Lucida Console", 28.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_scale_calc.ForeColor = System.Drawing.Color.Orange;
            this.label_scale_calc.Location = new System.Drawing.Point(303, 165);
            this.label_scale_calc.Name = "label_scale_calc";
            this.label_scale_calc.Size = new System.Drawing.Size(155, 38);
            this.label_scale_calc.TabIndex = 22;
            this.label_scale_calc.Text = "000000";
            this.label_scale_calc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_scale_calc.Click += new System.EventHandler(this.label_scale_calc_Click);
            // 
            // button_duration_minus
            // 
            this.button_duration_minus.ForeColor = System.Drawing.Color.Orange;
            this.button_duration_minus.Location = new System.Drawing.Point(10, 188);
            this.button_duration_minus.Name = "button_duration_minus";
            this.button_duration_minus.Size = new System.Drawing.Size(50, 30);
            this.button_duration_minus.TabIndex = 21;
            this.button_duration_minus.Text = "-";
            this.button_duration_minus.UseVisualStyleBackColor = true;
            this.button_duration_minus.Click += new System.EventHandler(this.button_duration_minus_Click);
            // 
            // button_duration_plus
            // 
            this.button_duration_plus.ForeColor = System.Drawing.Color.Orange;
            this.button_duration_plus.Location = new System.Drawing.Point(10, 152);
            this.button_duration_plus.Name = "button_duration_plus";
            this.button_duration_plus.Size = new System.Drawing.Size(50, 30);
            this.button_duration_plus.TabIndex = 20;
            this.button_duration_plus.Text = "+";
            this.button_duration_plus.UseVisualStyleBackColor = true;
            this.button_duration_plus.Click += new System.EventHandler(this.button_duration_plus_Click);
            // 
            // label_duration_calc
            // 
            this.label_duration_calc.AutoSize = true;
            this.label_duration_calc.Font = new System.Drawing.Font("Lucida Console", 28.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_duration_calc.ForeColor = System.Drawing.Color.Orange;
            this.label_duration_calc.Location = new System.Drawing.Point(92, 165);
            this.label_duration_calc.Name = "label_duration_calc";
            this.label_duration_calc.Size = new System.Drawing.Size(109, 38);
            this.label_duration_calc.TabIndex = 19;
            this.label_duration_calc.Text = "000 ";
            this.label_duration_calc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_duration_calc.Click += new System.EventHandler(this.label_duration_calc_Click);
            // 
            // button_freq_minus
            // 
            this.button_freq_minus.ForeColor = System.Drawing.Color.Orange;
            this.button_freq_minus.Location = new System.Drawing.Point(10, 81);
            this.button_freq_minus.Name = "button_freq_minus";
            this.button_freq_minus.Size = new System.Drawing.Size(50, 30);
            this.button_freq_minus.TabIndex = 18;
            this.button_freq_minus.Text = "-";
            this.button_freq_minus.UseVisualStyleBackColor = true;
            this.button_freq_minus.Click += new System.EventHandler(this.button_freq_minus_Click);
            // 
            // button_freq_plus
            // 
            this.button_freq_plus.Font = new System.Drawing.Font("Calibri Light", 16.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_freq_plus.ForeColor = System.Drawing.Color.Orange;
            this.button_freq_plus.Location = new System.Drawing.Point(10, 46);
            this.button_freq_plus.Name = "button_freq_plus";
            this.button_freq_plus.Size = new System.Drawing.Size(50, 30);
            this.button_freq_plus.TabIndex = 17;
            this.button_freq_plus.Text = "+";
            this.button_freq_plus.UseVisualStyleBackColor = true;
            this.button_freq_plus.Click += new System.EventHandler(this.button_freq_plus_Click_1);
            // 
            // label_freq_calc
            // 
            this.label_freq_calc.AutoSize = true;
            this.label_freq_calc.Font = new System.Drawing.Font("Lucida Console", 50.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_freq_calc.ForeColor = System.Drawing.Color.Orange;
            this.label_freq_calc.Location = new System.Drawing.Point(89, 46);
            this.label_freq_calc.Name = "label_freq_calc";
            this.label_freq_calc.Size = new System.Drawing.Size(349, 67);
            this.label_freq_calc.TabIndex = 16;
            this.label_freq_calc.Text = "00000000";
            this.label_freq_calc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_freq_calc.Click += new System.EventHandler(this.label_freq_calc_Click);
            // 
            // button_set_default
            // 
            this.button_set_default.Location = new System.Drawing.Point(454, 176);
            this.button_set_default.Name = "button_set_default";
            this.button_set_default.Size = new System.Drawing.Size(0, 0);
            this.button_set_default.TabIndex = 15;
            this.button_set_default.Text = "50 50%";
            this.button_set_default.UseVisualStyleBackColor = true;
            this.button_set_default.Visible = false;
            this.button_set_default.Click += new System.EventHandler(this.button_set_default_Click);
            // 
            // groupBox_MRC
            // 
            this.groupBox_MRC.Controls.Add(this.label_duty_cycle);
            this.groupBox_MRC.Controls.Add(this.label_duty_cycle_l);
            this.groupBox_MRC.Controls.Add(this.label_pause);
            this.groupBox_MRC.Controls.Add(this.label12);
            this.groupBox_MRC.Controls.Add(this.label7);
            this.groupBox_MRC.Controls.Add(this.label5);
            this.groupBox_MRC.Controls.Add(this.numericUpDown_ccr2);
            this.groupBox_MRC.Controls.Add(this.label_period);
            this.groupBox_MRC.Controls.Add(this.label8);
            this.groupBox_MRC.Controls.Add(this.numericUpDown_ccr1);
            this.groupBox_MRC.Controls.Add(this.label_pulse);
            this.groupBox_MRC.Controls.Add(this.label11);
            this.groupBox_MRC.Controls.Add(this.numericUpDown_psc);
            this.groupBox_MRC.Controls.Add(this.label_gen_clock);
            this.groupBox_MRC.Controls.Add(this.label6);
            this.groupBox_MRC.Controls.Add(this.numericUpDown_arr);
            this.groupBox_MRC.Controls.Add(this.label4);
            this.groupBox_MRC.Controls.Add(this.button7);
            this.groupBox_MRC.Controls.Add(this.button8);
            this.groupBox_MRC.Controls.Add(this.label9);
            this.groupBox_MRC.Location = new System.Drawing.Point(12, 293);
            this.groupBox_MRC.Name = "groupBox_MRC";
            this.groupBox_MRC.Size = new System.Drawing.Size(502, 231);
            this.groupBox_MRC.TabIndex = 25;
            this.groupBox_MRC.TabStop = false;
            this.groupBox_MRC.Text = "Manual registers control";
            // 
            // label_duty_cycle
            // 
            this.label_duty_cycle.AutoSize = true;
            this.label_duty_cycle.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_duty_cycle.ForeColor = System.Drawing.Color.Orange;
            this.label_duty_cycle.Location = new System.Drawing.Point(332, 161);
            this.label_duty_cycle.Name = "label_duty_cycle";
            this.label_duty_cycle.Size = new System.Drawing.Size(58, 16);
            this.label_duty_cycle.TabIndex = 43;
            this.label_duty_cycle.Text = "65535";
            this.label_duty_cycle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_duty_cycle_l
            // 
            this.label_duty_cycle_l.AutoSize = true;
            this.label_duty_cycle_l.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_duty_cycle_l.ForeColor = System.Drawing.Color.Orange;
            this.label_duty_cycle_l.Location = new System.Drawing.Point(157, 161);
            this.label_duty_cycle_l.Name = "label_duty_cycle_l";
            this.label_duty_cycle_l.Size = new System.Drawing.Size(138, 16);
            this.label_duty_cycle_l.TabIndex = 42;
            this.label_duty_cycle_l.Text = "Duty cycle, %";
            this.label_duty_cycle_l.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_pause
            // 
            this.label_pause.AutoSize = true;
            this.label_pause.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_pause.ForeColor = System.Drawing.Color.Orange;
            this.label_pause.Location = new System.Drawing.Point(157, 130);
            this.label_pause.Name = "label_pause";
            this.label_pause.Size = new System.Drawing.Size(88, 16);
            this.label_pause.TabIndex = 41;
            this.label_pause.Text = "Pause, S";
            this.label_pause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label12.ForeColor = System.Drawing.Color.Orange;
            this.label12.Location = new System.Drawing.Point(157, 104);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 40;
            this.label12.Text = "Pusle, S";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label7.ForeColor = System.Drawing.Color.Orange;
            this.label7.Location = new System.Drawing.Point(157, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 16);
            this.label7.TabIndex = 39;
            this.label7.Text = "Period, S";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label5.ForeColor = System.Drawing.Color.Orange;
            this.label5.Location = new System.Drawing.Point(157, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(148, 16);
            this.label5.TabIndex = 38;
            this.label5.Text = "Core clock, Hz";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_ccr2
            // 
            this.numericUpDown_ccr2.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_ccr2.Location = new System.Drawing.Point(60, 97);
            this.numericUpDown_ccr2.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown_ccr2.Name = "numericUpDown_ccr2";
            this.numericUpDown_ccr2.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_ccr2.TabIndex = 35;
            this.numericUpDown_ccr2.ValueChanged += new System.EventHandler(this.numericUpDown_ccr2_ValueChanged);
            // 
            // label_period
            // 
            this.label_period.AutoSize = true;
            this.label_period.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_period.ForeColor = System.Drawing.Color.Orange;
            this.label_period.Location = new System.Drawing.Point(332, 78);
            this.label_period.Name = "label_period";
            this.label_period.Size = new System.Drawing.Size(58, 16);
            this.label_period.TabIndex = 37;
            this.label_period.Text = "65535";
            this.label_period.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_period.Click += new System.EventHandler(this.numericUpDown_psc_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.DarkOrange;
            this.label8.Location = new System.Drawing.Point(7, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 36;
            this.label8.Text = "CCR2";
            // 
            // numericUpDown_ccr1
            // 
            this.numericUpDown_ccr1.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_ccr1.Location = new System.Drawing.Point(60, 71);
            this.numericUpDown_ccr1.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown_ccr1.Name = "numericUpDown_ccr1";
            this.numericUpDown_ccr1.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_ccr1.TabIndex = 30;
            this.numericUpDown_ccr1.ValueChanged += new System.EventHandler(this.numericUpDown_ccr1_ValueChanged);
            // 
            // label_pulse
            // 
            this.label_pulse.AutoSize = true;
            this.label_pulse.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_pulse.ForeColor = System.Drawing.Color.Orange;
            this.label_pulse.Location = new System.Drawing.Point(332, 104);
            this.label_pulse.Name = "label_pulse";
            this.label_pulse.Size = new System.Drawing.Size(58, 16);
            this.label_pulse.TabIndex = 32;
            this.label_pulse.Text = "65535";
            this.label_pulse.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.DarkOrange;
            this.label11.Location = new System.Drawing.Point(7, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 16);
            this.label11.TabIndex = 31;
            this.label11.Text = "CCR1";
            // 
            // numericUpDown_psc
            // 
            this.numericUpDown_psc.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_psc.Location = new System.Drawing.Point(60, 45);
            this.numericUpDown_psc.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown_psc.Name = "numericUpDown_psc";
            this.numericUpDown_psc.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_psc.TabIndex = 25;
            this.numericUpDown_psc.ValueChanged += new System.EventHandler(this.numericUpDown_psc_ValueChanged);
            // 
            // label_gen_clock
            // 
            this.label_gen_clock.AutoSize = true;
            this.label_gen_clock.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label_gen_clock.ForeColor = System.Drawing.Color.Orange;
            this.label_gen_clock.Location = new System.Drawing.Point(332, 20);
            this.label_gen_clock.Name = "label_gen_clock";
            this.label_gen_clock.Size = new System.Drawing.Size(58, 16);
            this.label_gen_clock.TabIndex = 27;
            this.label_gen_clock.Text = "65535";
            this.label_gen_clock.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_gen_clock.Click += new System.EventHandler(this.label_gen_clock_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.DarkOrange;
            this.label6.Location = new System.Drawing.Point(7, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "PSC";
            // 
            // numericUpDown_arr
            // 
            this.numericUpDown_arr.BackColor = System.Drawing.Color.Orange;
            this.numericUpDown_arr.Location = new System.Drawing.Point(60, 19);
            this.numericUpDown_arr.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numericUpDown_arr.Name = "numericUpDown_arr";
            this.numericUpDown_arr.Size = new System.Drawing.Size(80, 20);
            this.numericUpDown_arr.TabIndex = 12;
            this.numericUpDown_arr.ValueChanged += new System.EventHandler(this.numericUpDown_arr_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Console", 12F);
            this.label4.ForeColor = System.Drawing.Color.Orange;
            this.label4.Location = new System.Drawing.Point(332, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "65535";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(9, 183);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(42, 42);
            this.button7.TabIndex = 15;
            this.button7.Text = "50 50%";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Red;
            this.button8.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button8.Location = new System.Drawing.Point(57, 183);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(42, 42);
            this.button8.TabIndex = 9;
            this.button8.Text = "0\r\n";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Console", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.DarkOrange;
            this.label9.Location = new System.Drawing.Point(7, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 16);
            this.label9.TabIndex = 13;
            this.label9.Text = "ARR";
            // 
            // winform_LL_EBD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.ClientSize = new System.Drawing.Size(525, 533);
            this.Controls.Add(this.groupBox_MRC);
            this.Controls.Add(this.groupBox_SP);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "winform_LL_EBD";
            this.Text = "Generator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_freq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_duration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_scale)).EndInit();
            this.groupBox_SP.ResumeLayout(false);
            this.groupBox_SP.PerformLayout();
            this.groupBox_MRC.ResumeLayout(false);
            this.groupBox_MRC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ccr2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ccr1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_psc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_arr)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_baudrate;
        private System.Windows.Forms.Label label_RX;
        private System.Windows.Forms.Label label_TX;
        private System.Windows.Forms.ComboBox comboBox_ports;
        private System.Windows.Forms.Button open_port;
        private System.Windows.Forms.Label label_port_status;
        private System.Windows.Forms.Button close_port;
        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.NumericUpDown numericUpDown_freq;
        private System.Windows.Forms.NumericUpDown numericUpDown_duration;
        private System.Windows.Forms.NumericUpDown numericUpDown_scale;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox_SP;
        private System.Windows.Forms.Label label_freq_calc;
        private System.Windows.Forms.Button button_set_default;
        private System.Windows.Forms.Button button_freq_minus;
        private System.Windows.Forms.Button button_freq_plus;
        private System.Windows.Forms.Button button_duration_minus;
        private System.Windows.Forms.Button button_duration_plus;
        private System.Windows.Forms.Label label_duration_calc;
        private System.Windows.Forms.Button button_scale_minus;
        private System.Windows.Forms.Button button_scale_plus;
        private System.Windows.Forms.Label label_scale_calc;
        private System.Windows.Forms.GroupBox groupBox_MRC;
        private System.Windows.Forms.NumericUpDown numericUpDown_psc;
        private System.Windows.Forms.Label label_gen_clock;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown_arr;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDown_ccr2;
        private System.Windows.Forms.Label label_period;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown_ccr1;
        private System.Windows.Forms.Label label_pulse;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label_MRC;
        private System.Windows.Forms.Label label_Pulse_1kHz;
        private System.Windows.Forms.Label label_pulse_50Hz;
        private System.Windows.Forms.Label label_Pulse_1MHz;
        private System.Windows.Forms.Label label_pulse_1Hz;
        private System.Windows.Forms.Label label_Pulse_440Hz;
        private System.Windows.Forms.Label label_Pulse_10MHz;
        private System.Windows.Forms.Label label_duration_100;
        private System.Windows.Forms.Label label_duration_75;
        private System.Windows.Forms.Label label_duration_25;
        private System.Windows.Forms.Label label_duration_0;
        private System.Windows.Forms.Label label_duration_50;
        private System.Windows.Forms.Label label_rabbit;
        private System.Windows.Forms.Label label_Pulse_10kHz;
        private System.Windows.Forms.Label label_scale_1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_duty_cycle;
        private System.Windows.Forms.Label label_duty_cycle_l;
        private System.Windows.Forms.Label label_pause;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label_dir;
    }
}

