﻿using System;
using System.Timers;
using System.Threading;
using System.Windows.Forms;
using l420;
using micro.sdk;

using System.Collections;
using System.Globalization;

namespace app
{
    public partial class winform_LL_FOO : Form
    {

        System.Windows.Forms.Timer _timer;
        public winform_LL_FOO()
        {
            InitializeComponent();
            
            intercom.eventDataEncoded += IRQ_PackEncoded;


            _timer = new System.Windows.Forms.Timer();
            _timer.Interval = 1100;
            _timer.Tick += _timer_Tick;
            _timer.Start();
        }
        private void _timer_Tick(object sender, EventArgs e)
        {
            /// 
  
        }

        /// <summary>
        /// 
        /// Ответ от ответной части proto_R3C0NF1GUR4T0R
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="data"></param>
        public void IRQ_PackEncoded(byte opcode, byte[] data)
        {
            var raw_str = System.Text.Encoding.Default.GetString(data);
            log.print(raw_str);

            /// !!! cross thread update
            Invoke(new Action(() =>
            {
                
            }));
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void winform_R3C0NF1GUR4T0R_Load(object sender, EventArgs e)
        {

        }

        private void txt_SR_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
