﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Globalization;
using l420;
using micro.sdk;

namespace Generator
{
    public partial class winform_LL_Generator : Form
    {
        #region Serial
        private static SerialPort Serial = new SerialPort();
        const int ReadTimeout = 80;
        const int WriteTimeout = 40;
        #endregion Serial
        //EBD.Delivery port;
        EBD.Delivery port = new EBD.Delivery(Serial, 0x55);

        // Connection status and etc
        System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();

 

        public winform_LL_Generator()
        {
            InitializeComponent();
            
            intercom.eventDataEncoded += IRQ_PackEncoded;

            _timer.Interval = 250;
            _timer.Tick += UpdateTick;
            _timer.Start();


        }


        /// <summary>
        /// 
        /// Ответ от ответной части proto_R3C0NF1GUR4T0R
        /// 
        /// </summary>
        /// <param name="opcode"></param>
        /// <param name="data"></param>
        public void IRQ_PackEncoded(byte opcode, byte[] data)
        {
            var raw_str = System.Text.Encoding.Default.GetString(data);
            log.print(raw_str);


            if (data.Length < 40) return;

            /// !!! cross thread update
            Invoke(new Action(() =>
            {
                txtGen1_stepcount.Text = BitConverter.ToInt64(data, 2).ToString();
                txtGen2_stepcount.Text = BitConverter.ToInt64(data, 11).ToString();
                txtGen3_stepcount.Text = BitConverter.ToInt64(data, 20).ToString();
                txtGen4_stepcount.Text = BitConverter.ToInt64(data, 29).ToString();
            }));
        }

        // Update timer
        private void UpdateTick(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF0;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
            intercom.PACK_END();
        }

        private void open_port_Click(object sender, EventArgs e)
        {

        }

        private void close_port_Click(object sender, EventArgs e)
        {

        }





        private void button_reset_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF2;
            intercom.WRITE = (byte)0x01;
            intercom.PACK_END();

        }

        private void numericUpDown_freq_ValueChanged(object sender, EventArgs e)
        {
            //port.set_int((int)gen_regs.freq_set, (int)numericUpDown_freq.Value);
        }

        private void numericUpDown_duration_ValueChanged(object sender, EventArgs e)
        {
            //port.set_float((int)gen_regs.duration, (float)numericUpDown_duration.Value);
        }

        private void numericUpDown_scale_ValueChanged(object sender, EventArgs e)
        {
            //port.set_int((int)gen_regs.scale, (int)numericUpDown_scale.Value);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }


        private void _delayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //numericUpDown_duration.Value = 0;
            _timer.Stop();

            //port.set_int((int)gen_regs.freq_set, 0); Thread.Sleep(20);
            //port.set_int((int)gen_regs.duration, 0); Thread.Sleep(20);
            //port.set_int((int)gen_regs.scale, 0); Thread.Sleep(20);


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //numericUpDown_duration.Value = 50;
        }

        private void label_port_status_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown_duration_ValueChanged(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //port.set_int((int)gen_regs.freq_set, (int)Convert.ToInt32(textBox1.Text));   
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //port.set_int((int)gen_regs.scale, (int)Convert.ToInt32(textBox2.Text));
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            //port.set_float((int)gen_regs.duration, float.Parse(textBox3.Text,CultureInfo.InvariantCulture));
        }

        private void button8_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF1;                    // OPCODE PWM_CHANGE
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x01;
            intercom.WRITE = byte.Parse(gen1_freg.Text);
            intercom.WRITE = byte.Parse(gen1_devider.Text);
            intercom.WRITE = byte.Parse(gen1_duration.Text);
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;
  
            intercom.PACK_END();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF1;// OPCODE PWM_CHANGE
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x02;
            intercom.WRITE = byte.Parse(gen2_freg.Text);
            intercom.WRITE = byte.Parse(gen2_devider.Text);
            intercom.WRITE = byte.Parse(gen2_duration.Text);
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;

            intercom.PACK_END();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF1;// OPCODE PWM_CHANGE
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x03;
            intercom.WRITE = byte.Parse(gen3_freg.Text);
            intercom.WRITE = byte.Parse(gen3_devider.Text);
            intercom.WRITE = byte.Parse(gen3_duration.Text);
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;

            intercom.PACK_END();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF1;// OPCODE PWM_CHANGE
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x04;
            intercom.WRITE = byte.Parse(gen4_freg.Text);
            intercom.WRITE = byte.Parse(gen4_devider.Text);
            intercom.WRITE = byte.Parse(gen4_duration.Text);
            intercom.WRITE = (byte)0x00;
            intercom.WRITE = (byte)0x00;

            intercom.PACK_END();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF3;
            intercom.WRITE = (byte)0x01;
            intercom.PACK_END();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF2;
            intercom.WRITE = (byte)0x02;
            intercom.PACK_END();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF3;
            intercom.WRITE = (byte)0x02;
            intercom.PACK_END();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF2;
            intercom.WRITE = (byte)0x03;
            intercom.PACK_END();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF3;
            intercom.WRITE = (byte)0x03;
            intercom.PACK_END();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF2;
            intercom.WRITE = (byte)0x04;
            intercom.PACK_END();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            intercom.PACK_BEGIN();
            intercom.WRITE = (byte)0xF3;
            intercom.WRITE = (byte)0x04;
            intercom.PACK_END();
        }
    }

}
