﻿namespace Generator
{
    partial class winform_LL_Generator
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_reset = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGen1_duration = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.gen1_duration = new System.Windows.Forms.TextBox();
            this.gen1_devider = new System.Windows.Forms.TextBox();
            this.gen1_freg = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtGen1_stepcount = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtGen2_stepcount = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gen2_duration = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.gen2_devider = new System.Windows.Forms.TextBox();
            this.gen2_freg = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtGen3_stepcount = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.gen3_duration = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.gen3_devider = new System.Windows.Forms.TextBox();
            this.gen3_freg = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtGen4_stepcount = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.gen4_duration = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.gen4_devider = new System.Windows.Forms.TextBox();
            this.gen4_freg = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_reset
            // 
            this.button_reset.Location = new System.Drawing.Point(41, 463);
            this.button_reset.Margin = new System.Windows.Forms.Padding(7);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(467, 62);
            this.button_reset.TabIndex = 9;
            this.button_reset.Text = "STOP";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(5, 129);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(262, 31);
            this.label2.TabIndex = 13;
            this.label2.Text = "frequency_devider";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtGen1_duration
            // 
            this.txtGen1_duration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGen1_duration.Location = new System.Drawing.Point(0, 196);
            this.txtGen1_duration.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.txtGen1_duration.Name = "txtGen1_duration";
            this.txtGen1_duration.Size = new System.Drawing.Size(268, 31);
            this.txtGen1_duration.TabIndex = 14;
            this.txtGen1_duration.Text = "duration %";
            this.txtGen1_duration.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtGen1_duration.Click += new System.EventHandler(this.label3_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtGen1_duration);
            this.groupBox2.Controls.Add(this.gen1_duration);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.gen1_devider);
            this.groupBox2.Controls.Add(this.gen1_freg);
            this.groupBox2.Location = new System.Drawing.Point(41, 71);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox2.Size = new System.Drawing.Size(467, 254);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ГЕНЕРАТОР_1";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(0, 63);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(268, 31);
            this.label1.TabIndex = 16;
            this.label1.Text = "frequency";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen1_duration
            // 
            this.gen1_duration.Location = new System.Drawing.Point(310, 190);
            this.gen1_duration.Margin = new System.Windows.Forms.Padding(5);
            this.gen1_duration.Name = "gen1_duration";
            this.gen1_duration.Size = new System.Drawing.Size(114, 35);
            this.gen1_duration.TabIndex = 2;
            this.gen1_duration.Text = "50";
            this.gen1_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gen1_duration.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // gen1_devider
            // 
            this.gen1_devider.Location = new System.Drawing.Point(310, 123);
            this.gen1_devider.Margin = new System.Windows.Forms.Padding(5);
            this.gen1_devider.Name = "gen1_devider";
            this.gen1_devider.Size = new System.Drawing.Size(114, 35);
            this.gen1_devider.TabIndex = 1;
            this.gen1_devider.Text = "1";
            this.gen1_devider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gen1_devider.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // gen1_freg
            // 
            this.gen1_freg.Location = new System.Drawing.Point(310, 58);
            this.gen1_freg.Margin = new System.Windows.Forms.Padding(5);
            this.gen1_freg.Name = "gen1_freg";
            this.gen1_freg.Size = new System.Drawing.Size(114, 35);
            this.gen1_freg.TabIndex = 0;
            this.gen1_freg.Text = "4";
            this.gen1_freg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gen1_freg.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(41, 539);
            this.button1.Margin = new System.Windows.Forms.Padding(7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(467, 62);
            this.button1.TabIndex = 18;
            this.button1.Text = "START";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtGen1_stepcount);
            this.groupBox1.Location = new System.Drawing.Point(41, 622);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(467, 130);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "СЧЕТЧИК";
            // 
            // txtGen1_stepcount
            // 
            this.txtGen1_stepcount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGen1_stepcount.Location = new System.Drawing.Point(0, 62);
            this.txtGen1_stepcount.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.txtGen1_stepcount.Name = "txtGen1_stepcount";
            this.txtGen1_stepcount.Size = new System.Drawing.Size(453, 40);
            this.txtGen1_stepcount.TabIndex = 17;
            this.txtGen1_stepcount.Text = "99999999999";
            this.txtGen1_stepcount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtGen1_stepcount.UseMnemonic = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtGen2_stepcount);
            this.groupBox3.Location = new System.Drawing.Point(570, 622);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(467, 130);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "СЧЕТЧИК";
            // 
            // txtGen2_stepcount
            // 
            this.txtGen2_stepcount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGen2_stepcount.Location = new System.Drawing.Point(0, 62);
            this.txtGen2_stepcount.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.txtGen2_stepcount.Name = "txtGen2_stepcount";
            this.txtGen2_stepcount.Size = new System.Drawing.Size(453, 40);
            this.txtGen2_stepcount.TabIndex = 17;
            this.txtGen2_stepcount.Text = "99999999999";
            this.txtGen2_stepcount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtGen2_stepcount.UseMnemonic = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(570, 527);
            this.button2.Margin = new System.Windows.Forms.Padding(7);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(467, 62);
            this.button2.TabIndex = 22;
            this.button2.Text = "START";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(570, 451);
            this.button3.Margin = new System.Windows.Forms.Padding(7);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(467, 62);
            this.button3.TabIndex = 19;
            this.button3.Text = "STOP";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.gen2_duration);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.gen2_devider);
            this.groupBox4.Controls.Add(this.gen2_freg);
            this.groupBox4.Location = new System.Drawing.Point(570, 71);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox4.Size = new System.Drawing.Size(467, 254);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "ГЕНЕРАТОР_2";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.Location = new System.Drawing.Point(0, 63);
            this.label6.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(268, 31);
            this.label6.TabIndex = 16;
            this.label6.Text = "frequency";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Location = new System.Drawing.Point(0, 196);
            this.label7.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(268, 31);
            this.label7.TabIndex = 14;
            this.label7.Text = "duration %";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen2_duration
            // 
            this.gen2_duration.Location = new System.Drawing.Point(310, 190);
            this.gen2_duration.Margin = new System.Windows.Forms.Padding(5);
            this.gen2_duration.Name = "gen2_duration";
            this.gen2_duration.Size = new System.Drawing.Size(114, 35);
            this.gen2_duration.TabIndex = 2;
            this.gen2_duration.Text = "50";
            this.gen2_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.Location = new System.Drawing.Point(5, 129);
            this.label8.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(262, 31);
            this.label8.TabIndex = 13;
            this.label8.Text = "frequency_devider";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen2_devider
            // 
            this.gen2_devider.Location = new System.Drawing.Point(310, 123);
            this.gen2_devider.Margin = new System.Windows.Forms.Padding(5);
            this.gen2_devider.Name = "gen2_devider";
            this.gen2_devider.Size = new System.Drawing.Size(114, 35);
            this.gen2_devider.TabIndex = 1;
            this.gen2_devider.Text = "1";
            this.gen2_devider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gen2_freg
            // 
            this.gen2_freg.Location = new System.Drawing.Point(310, 58);
            this.gen2_freg.Margin = new System.Windows.Forms.Padding(5);
            this.gen2_freg.Name = "gen2_freg";
            this.gen2_freg.Size = new System.Drawing.Size(114, 35);
            this.gen2_freg.TabIndex = 0;
            this.gen2_freg.Text = "4";
            this.gen2_freg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtGen3_stepcount);
            this.groupBox5.Location = new System.Drawing.Point(1092, 622);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox5.Size = new System.Drawing.Size(467, 130);
            this.groupBox5.TabIndex = 25;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "СЧЕТЧИК";
            // 
            // txtGen3_stepcount
            // 
            this.txtGen3_stepcount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGen3_stepcount.Location = new System.Drawing.Point(0, 62);
            this.txtGen3_stepcount.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.txtGen3_stepcount.Name = "txtGen3_stepcount";
            this.txtGen3_stepcount.Size = new System.Drawing.Size(453, 40);
            this.txtGen3_stepcount.TabIndex = 17;
            this.txtGen3_stepcount.Text = "99999999999";
            this.txtGen3_stepcount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtGen3_stepcount.UseMnemonic = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1092, 527);
            this.button4.Margin = new System.Windows.Forms.Padding(7);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(467, 62);
            this.button4.TabIndex = 26;
            this.button4.Text = "START";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(1092, 451);
            this.button5.Margin = new System.Windows.Forms.Padding(7);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(467, 62);
            this.button5.TabIndex = 23;
            this.button5.Text = "STOP";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.gen3_duration);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.gen3_devider);
            this.groupBox6.Controls.Add(this.gen3_freg);
            this.groupBox6.Location = new System.Drawing.Point(1092, 84);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox6.Size = new System.Drawing.Size(467, 254);
            this.groupBox6.TabIndex = 24;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ГЕНЕРАТОР_3";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Location = new System.Drawing.Point(0, 63);
            this.label10.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(268, 31);
            this.label10.TabIndex = 16;
            this.label10.Text = "frequency";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.Location = new System.Drawing.Point(0, 196);
            this.label11.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(268, 31);
            this.label11.TabIndex = 14;
            this.label11.Text = "duration %";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen3_duration
            // 
            this.gen3_duration.Location = new System.Drawing.Point(310, 190);
            this.gen3_duration.Margin = new System.Windows.Forms.Padding(5);
            this.gen3_duration.Name = "gen3_duration";
            this.gen3_duration.Size = new System.Drawing.Size(114, 35);
            this.gen3_duration.TabIndex = 2;
            this.gen3_duration.Text = "50";
            this.gen3_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Location = new System.Drawing.Point(5, 129);
            this.label12.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(262, 31);
            this.label12.TabIndex = 13;
            this.label12.Text = "frequency_devider";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen3_devider
            // 
            this.gen3_devider.Location = new System.Drawing.Point(310, 123);
            this.gen3_devider.Margin = new System.Windows.Forms.Padding(5);
            this.gen3_devider.Name = "gen3_devider";
            this.gen3_devider.Size = new System.Drawing.Size(114, 35);
            this.gen3_devider.TabIndex = 1;
            this.gen3_devider.Text = "1";
            this.gen3_devider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gen3_freg
            // 
            this.gen3_freg.Location = new System.Drawing.Point(310, 58);
            this.gen3_freg.Margin = new System.Windows.Forms.Padding(5);
            this.gen3_freg.Name = "gen3_freg";
            this.gen3_freg.Size = new System.Drawing.Size(114, 35);
            this.gen3_freg.TabIndex = 0;
            this.gen3_freg.Text = "4";
            this.gen3_freg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtGen4_stepcount);
            this.groupBox7.Location = new System.Drawing.Point(1612, 622);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox7.Size = new System.Drawing.Size(467, 130);
            this.groupBox7.TabIndex = 29;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "СЧЕТЧИК";
            // 
            // txtGen4_stepcount
            // 
            this.txtGen4_stepcount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGen4_stepcount.Location = new System.Drawing.Point(0, 62);
            this.txtGen4_stepcount.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.txtGen4_stepcount.Name = "txtGen4_stepcount";
            this.txtGen4_stepcount.Size = new System.Drawing.Size(453, 40);
            this.txtGen4_stepcount.TabIndex = 17;
            this.txtGen4_stepcount.Text = "99999999999";
            this.txtGen4_stepcount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.txtGen4_stepcount.UseMnemonic = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(1602, 518);
            this.button6.Margin = new System.Windows.Forms.Padding(7);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(467, 62);
            this.button6.TabIndex = 30;
            this.button6.Text = "START";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(1602, 442);
            this.button7.Margin = new System.Windows.Forms.Padding(7);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(467, 62);
            this.button7.TabIndex = 27;
            this.button7.Text = "STOP";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label14);
            this.groupBox8.Controls.Add(this.label15);
            this.groupBox8.Controls.Add(this.gen4_duration);
            this.groupBox8.Controls.Add(this.label16);
            this.groupBox8.Controls.Add(this.gen4_devider);
            this.groupBox8.Controls.Add(this.gen4_freg);
            this.groupBox8.Location = new System.Drawing.Point(1602, 93);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox8.Size = new System.Drawing.Size(467, 254);
            this.groupBox8.TabIndex = 28;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "ГЕНЕРАТОР_4";
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.Location = new System.Drawing.Point(0, 63);
            this.label14.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(268, 31);
            this.label14.TabIndex = 16;
            this.label14.Text = "frequency";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.Location = new System.Drawing.Point(0, 196);
            this.label15.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(268, 31);
            this.label15.TabIndex = 14;
            this.label15.Text = "duration %";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen4_duration
            // 
            this.gen4_duration.Location = new System.Drawing.Point(310, 190);
            this.gen4_duration.Margin = new System.Windows.Forms.Padding(5);
            this.gen4_duration.Name = "gen4_duration";
            this.gen4_duration.Size = new System.Drawing.Size(114, 35);
            this.gen4_duration.TabIndex = 2;
            this.gen4_duration.Text = "50";
            this.gen4_duration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.Location = new System.Drawing.Point(5, 129);
            this.label16.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(262, 31);
            this.label16.TabIndex = 13;
            this.label16.Text = "frequency_devider";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gen4_devider
            // 
            this.gen4_devider.Location = new System.Drawing.Point(310, 120);
            this.gen4_devider.Margin = new System.Windows.Forms.Padding(5);
            this.gen4_devider.Name = "gen4_devider";
            this.gen4_devider.Size = new System.Drawing.Size(114, 35);
            this.gen4_devider.TabIndex = 1;
            this.gen4_devider.Text = "1";
            this.gen4_devider.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gen4_freg
            // 
            this.gen4_freg.Location = new System.Drawing.Point(310, 58);
            this.gen4_freg.Margin = new System.Windows.Forms.Padding(5);
            this.gen4_freg.Name = "gen4_freg";
            this.gen4_freg.Size = new System.Drawing.Size(114, 35);
            this.gen4_freg.TabIndex = 0;
            this.gen4_freg.Text = "4";
            this.gen4_freg.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(41, 337);
            this.button8.Margin = new System.Windows.Forms.Padding(7);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(467, 62);
            this.button8.TabIndex = 31;
            this.button8.Text = "CHANGE";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(570, 337);
            this.button9.Margin = new System.Windows.Forms.Padding(7);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(467, 62);
            this.button9.TabIndex = 32;
            this.button9.Text = "CHANGE";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(1097, 350);
            this.button10.Margin = new System.Windows.Forms.Padding(7);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(467, 62);
            this.button10.TabIndex = 33;
            this.button10.Text = "CHANGE";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(1602, 359);
            this.button11.Margin = new System.Windows.Forms.Padding(7);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(467, 62);
            this.button11.TabIndex = 34;
            this.button11.Text = "CHANGE";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // winform_LL_Generator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2123, 766);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button_reset);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "winform_LL_Generator";
            this.Text = "Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtGen1_duration;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox gen1_freg;
        private System.Windows.Forms.TextBox gen1_duration;
        private System.Windows.Forms.TextBox gen1_devider;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label txtGen1_stepcount;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label txtGen2_stepcount;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox gen2_duration;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox gen2_devider;
        private System.Windows.Forms.TextBox gen2_freg;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label txtGen3_stepcount;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox gen3_duration;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox gen3_devider;
        private System.Windows.Forms.TextBox gen3_freg;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label txtGen4_stepcount;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox gen4_duration;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox gen4_devider;
        private System.Windows.Forms.TextBox gen4_freg;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
    }
}

