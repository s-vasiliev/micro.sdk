﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Timers;

using l420;
using micro.sdk;

namespace app
{
    public partial class winform_LL_EBD : Form
    {
        #region Serial
        private static SerialPort Serial = intercom._serial;
        const int ReadTimeout = 80;
        const int WriteTimeout = 40;
        #endregion Serial
        //EBD.Delivery port;
        EBD.Delivery port = new EBD.Delivery(Serial, 0x55);

        // Connection status and etc
        System.Windows.Forms.Timer update = new System.Windows.Forms.Timer();

        public winform_LL_EBD()
        {
            InitializeComponent();
            this.ClientSize = new System.Drawing.Size(525, 300);
            if (Serial.IsOpen) Serial.Close();


            // serial service
            //intercom.eventDataEncoded += IRQ_PackEncoded;
            string[] ports = SerialPort.GetPortNames();

            comboBox_ports.Items.Clear();
            comboBox_ports.Items.AddRange(ports);
            comboBox_ports.SelectedIndex = 0;

            object[] bauds = { 115200, 230400, 921600 };
            comboBox_baudrate.Items.Clear();
            comboBox_baudrate.Items.AddRange(bauds);
            comboBox_baudrate.SelectedIndex = 0;

            // update timer
            update.Interval = 40;
            update.Tick += new EventHandler(UpdateTick);

            
        }

        // Update timer
        private void UpdateTick(object sender, EventArgs e)
        {
            try
            {
                label_port_status.Text = "Port: " + (Serial.IsOpen).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private void open_port_Click(object sender, EventArgs e)
        {
            
            if (Serial.IsOpen == false)
            {
                Serial.PortName = comboBox_ports.SelectedItem.ToString();
                Serial.BaudRate = (int)comboBox_baudrate.SelectedItem;
                Serial.ReadTimeout = ReadTimeout;
                Serial.WriteTimeout = WriteTimeout;
                Serial.Encoding = Encoding.GetEncoding("Windows-1251");

                //try
                //{
                Serial.Open();

                log.print("Port " + Serial.PortName.ToString() + " open @" + Serial.BaudRate.ToString());

                update.Start();
                port.set_int((int)gen_regs.freq_set, 0);
                port.set_int((int)gen_regs.freq_set, 1);
                int test = port.get_int((int)gen_regs.freq_set);
                port.set_int((int)gen_regs.freq_set, 0);
                if(test == 0)
                {
                    log.print_banner(2);
                }
                else
                {
                    log.print_banner(7);
                    numericUpDown_scale.Value = 1;
                    numericUpDown_freq.Value = 0;
                    numericUpDown_duration.Value = 50;
                    EventArgs ea = new EventArgs();
                    label_port_status_Click(sender, ea);
                    groupBox_MRC.Visible = !groupBox_MRC.Visible;
                    //this.ClientSize = new System.Drawing.Size(525, 300);
                }
                // }
                //catch { }
                //Serial.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceived);
                //port.PinChanged       += new SerialPinChangedEventHandler(port_pin_change);
                //EBD.Delivery port = new EBD.Delivery(Serial, 0x55);
                //numericUpDown_duration.Value = 50;
            }

        }

        private void close_port_Click(object sender, EventArgs e)
        {
            if (Serial.IsOpen == true)
            {
                Serial.Close();
                update.Stop();
            }
        }


        private enum gen_regs
{
            // basic registers
            hw_version = 0,   // 
            sw_version,
            mode,
            core_clock,
            // mode 0: frequency generator
            freq_set,
            duration,
            scale,
            // mode 1: manually timer control
            arr,
            cnt,
            ccr1,
            ccr2,
            ccr3,
            ccr4,
            psc,
            dir,
        }


        private void set_frequency(int freq)
        {
            // set new frequency
            port.set_int((int)gen_regs.freq_set, freq);
            // get value (check)
            freq = ((int)numericUpDown_scale.Value * port.get_int((int)gen_regs.freq_set));
            log.print(string.Format("Set frequency: {0:00000000}, Hz", freq));
            //Console.WriteLine("Set frequency: {0}, Hz", freq);
        }

        private void set_duration(float duration)
        {
            // set duration
            port.set_float((int)gen_regs.duration, duration);

            //string result = string.Format("Set duration: {0:00} %", duration);
            log.print(string.Format("Set duration: {0:00} %", duration));
            //Console.WriteLine("Set duration: {0}, %", duration);
        }

        private void set_scale(int scale)
        {
            // set new frequency
            port.set_int((int)gen_regs.scale, scale);
            log.print(string.Format("Set scale: {0}", scale));
            //Console.WriteLine("Set scale: {0}", scale);
        }



        private void numericUpDown_freq_ValueChanged(object sender, EventArgs e)
        {
            int freq = (int)numericUpDown_freq.Value;
            int scale = (int)numericUpDown_scale.Value;

            set_frequency(freq);
            int freq_from_generator = port.get_int((int)gen_regs.freq_set);


            label_freq_calc.Text = string.Format("{0:00000000}", scale * freq_from_generator);
        }

        private void numericUpDown_duration_ValueChanged(object sender, EventArgs e)
        {
            set_duration((float)numericUpDown_duration.Value);
            label_duration_calc.Text = string.Format("{0:000}", numericUpDown_duration.Value);
        }

        private void numericUpDown_scale_ValueChanged(object sender, EventArgs e)
        {
            int scale = (int)numericUpDown_scale.Value;
            set_scale(scale);
            int freq_from_generator = port.get_int((int)gen_regs.freq_set);
            label_scale_calc.Text = string.Format("{0:000000}", scale);
            label_freq_calc.Text = string.Format("{0:00000000}", scale * freq_from_generator);
        }


        private void button_set_default_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 50;
            numericUpDown_duration.Value = 50;

        }

        private void button_reset_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 0;
            numericUpDown_freq.Value = 0;
            numericUpDown_duration.Value = 0;
        }




        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label_freq_calc_Click(object sender, EventArgs e)
        {
            //numericUpDown_duration.Visible = !numericUpDown_duration.Visible;
            numericUpDown_freq.Visible = !numericUpDown_freq.Visible;
            //numericUpDown_scale.Visible = !numericUpDown_scale.Visible;
        }

        private void label_RX_Click(object sender, EventArgs e)
        {

        }

        private void label_port_status_Click(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();

            comboBox_ports.Items.Clear();
            comboBox_ports.Items.AddRange(ports);
            comboBox_ports.SelectedIndex = 0;

            object[] bauds = { 115200, 230400, 921600 };
            comboBox_baudrate.Items.Clear();
            comboBox_baudrate.Items.AddRange(bauds);
            comboBox_baudrate.SelectedIndex = 0;

            comboBox_ports.Visible = !comboBox_ports.Visible;
            comboBox_baudrate.Visible = !comboBox_baudrate.Visible;
            open_port.Visible = !open_port.Visible;
            close_port.Visible = !close_port.Visible;
        }

        private void button_freq_plus_Click_1(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_freq.Value++;
            }
            catch (Exception ex)
            {

            }
        }

        private void button_freq_minus_Click(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_freq.Value--;           
            }
            catch (Exception ex)
            {

            }
        }

        private void button_duration_plus_Click(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_duration.Value++;
            }
            catch (Exception ex)
            {

            }
        }

        private void button_duration_minus_Click(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_duration.Value--;
            }
            catch (Exception ex)
            {

            }
        }

        private void button_scale_plus_Click(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_scale.Value++;
            }
            catch (Exception ex)
            {

            }
        }

        private void button_scale_minus_Click(object sender, EventArgs e)
        {
            try
            {
                numericUpDown_scale.Value--;
            }
            catch (Exception ex)
            {

            }
        }

        private void label_scale_calc_Click(object sender, EventArgs e)
        {
            //numericUpDown_scale.Value = 1;
            numericUpDown_scale.Visible = !numericUpDown_scale.Visible;
        }

        private void label_duration_calc_Click(object sender, EventArgs e)
        {
            //numericUpDown_duration.Value = 50;
            numericUpDown_duration.Visible = !numericUpDown_duration.Visible;
        }

        private void label_MRC_Click(object sender, EventArgs e)
        {
            groupBox_MRC.Visible = !groupBox_MRC.Visible;
            if (groupBox_MRC.Visible)
            {
                port.set_uint((int)gen_regs.mode, 1);
                label_gen_clock.Text = port.get_int((int)gen_regs.core_clock).ToString();
                this.ClientSize = new System.Drawing.Size(525, 539);
            }
            else
            {
                port.set_uint((int)gen_regs.mode, 0);
                this.ClientSize = new System.Drawing.Size(525, 300);
            }

        }

        private void label_Pulse_1kHz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 1000;
            numericUpDown_duration.Value = 50;
        }

        private void label_pulse_50Hz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 50;
            numericUpDown_duration.Value = 50;
        }

        private void label_Pulse_1MHz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 100;
            numericUpDown_freq.Value = 10000;
            numericUpDown_duration.Value = 50;
        }

        private void label_pulse_1Hz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 1;
            numericUpDown_duration.Value = 50;
        }

        private void label_Pulse_440Hz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 440;
            numericUpDown_duration.Value = 50;
        }

        private void label_Pulse_10MHz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1000;
            numericUpDown_freq.Value = 10000;
            numericUpDown_duration.Value = 50;
        }

        private void label_duration_0_Click(object sender, EventArgs e)
        {
            numericUpDown_duration.Value = 0;
        }

        private void label_duration_25_Click(object sender, EventArgs e)
        {
            numericUpDown_duration.Value = 25;
        }

        private void label_duration_75_Click(object sender, EventArgs e)
        {
            numericUpDown_duration.Value = 75;
        }

        private void label_duration_100_Click(object sender, EventArgs e)
        {
            numericUpDown_duration.Value = 100;
        }

        private void label_duration_50_Click(object sender, EventArgs e)
        {
            numericUpDown_duration.Value = 50;
        }


        private void label_rabbit_Click(object sender, EventArgs e)
        {
            //numericUpDown_scale.Value = 1;
            //numericUpDown_freq.Value = 420;
            //numericUpDown_duration.Value = 42;
            port.set_uint((int)gen_regs.mode, 3);

        }

        private void label_Pulse_10kHz_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
            numericUpDown_freq.Value = 10000;
            numericUpDown_duration.Value = 50;
        }

        private void label_scale_1_Click(object sender, EventArgs e)
        {
            numericUpDown_scale.Value = 1;
        }




        // Manual registers control
        private void numericUpDown_arr_ValueChanged(object sender, EventArgs e)
        {
            uint arr = (uint)numericUpDown_arr.Value;
            port.set_uint((int)gen_regs.arr, arr);
        }

        private void numericUpDown_psc_ValueChanged(object sender, EventArgs e)
        {
            uint psc = (uint)numericUpDown_psc.Value;
            port.set_uint((int)gen_regs.psc, psc);
        }

        private void numericUpDown_ccr1_ValueChanged(object sender, EventArgs e)
        {
            uint ccr1 = (uint)numericUpDown_ccr1.Value;
            port.set_uint((int)gen_regs.ccr1, ccr1);
        }

        private void numericUpDown_ccr2_ValueChanged(object sender, EventArgs e)
        {
            uint ccr2 = (uint)numericUpDown_ccr2.Value;
            port.set_uint((int)gen_regs.ccr2, ccr2);
        }

        private void label_period_Click(object sender, EventArgs e)
        {
            try
            {
                label_period.Text = ((numericUpDown_arr.Value) / 80000000 * (numericUpDown_psc.Value + 1)).ToString();
            }catch(Exception ex)
            { 
           

            }
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label_gen_clock_Click(object sender, EventArgs e)
        {

        }
        private uint dir = 0;
        private void label_dir_Click(object sender, EventArgs e)
        {
            if (dir == 0)
            {
                dir = 1;
            }
            else
            {
                dir = 0;
            }
            port.set_uint((int)gen_regs.dir, dir);
        }
    }

}
