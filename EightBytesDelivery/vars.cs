﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using EightBytesDelivery;

namespace EBD
{
    public partial class Delivery
    {
        // Main structure
        private struct Instance
        {
            // Serial port
            public SerialPort serial;
            // RAW data storage
            public byte[] data_out;
            public byte[] data_in;
            public byte[] payload;
            public int Address;
            public Transfer_state state;
            public int rec_len;
            public Exception ex;
        }
        private Instance inst;

        // Enum of payload types
        private enum payload_type
        {
            Payload_float = 0,
            Payload_int,
            Payload_uint,
            Payload_pointer,  // aka uint
            Payload_bitfield,
            Payload_dual_angles,
        }


        // State of byte delivery
        private enum Transfer_state
        {
            send = 0,
            wait,
            exception,
            crc_error,
            len_error,
            OK
        }
        // RW constant for rw_reg
        private const int rw_bit = 0x80;
        // Payload offset on packet
        const int pl_offset = 2;
        // data field offset on packet
        private enum pack
        {
            header = 0,
            rw_reg,
            data_0,
            data_1,
            data_2,
            data_3,
            type,
            crc
        }

        // Dual angles struct
        public struct dual_angles
        {
            public int state_0;
            public int angle_0;
            public int state_1;
            public int angle_1;
        }
    }
}