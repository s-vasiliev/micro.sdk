﻿using System;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;
using EightBytesDelivery;

namespace EBD
{
    public partial class Delivery
    {
        // Use to change delivery address
        public void address_set(int address)
        {
            inst.Address = address;
        }
    }
}