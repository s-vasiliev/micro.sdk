﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using EightBytesDelivery;
using System.Threading;

namespace EBD
{
    public partial class Delivery 
    {
        // Constructor
        public Delivery(SerialPort Serial, int address = 0x55)
        {
            inst = new Instance();
            inst.data_in = new byte[8];
            inst.data_out = new byte[8];
            inst.payload = new byte[8];
            inst.Address = address;

            if (Serial == null)
            {

            }
            else
            {
                inst.serial = Serial;
                inst.serial.ReadTimeout = 50;
            }
        }

        // destructor
        ~Delivery()
        {
            inst.serial.Dispose();
            inst.serial.Close();
        }


        /// calculate crc8 for packet
        private byte packet_crc_calc(byte[] array)
        {
            return Crc8.ComputeChecksum(array[0], array[1], array[2], array[3], array[4], array[5], array[6]);
        }

        // Write float register
        public float set_float(int register, float value)
        {
            float result = 0;
            byte[] temp = BitConverter.GetBytes(value);

            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(rw_bit + register);
            inst.data_out[(int)pack.data_0] = temp[0];
            inst.data_out[(int)pack.data_1] = temp[1];
            inst.data_out[(int)pack.data_2] = temp[2];
            inst.data_out[(int)pack.data_3] = temp[3];
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_float;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                result = BitConverter.ToSingle(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }

        // Read float register
        public float get_float(int register)
        {
            float result = 0;
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(register);
            inst.data_out[(int)pack.data_0] = 0;
            inst.data_out[(int)pack.data_1] = 0;
            inst.data_out[(int)pack.data_2] = 0;
            inst.data_out[(int)pack.data_3] = 0;
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_float;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                result = BitConverter.ToSingle(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }

        // Write int register
        public int set_int(int register, int value)
        {
            int result = 0;
            byte[] temp = BitConverter.GetBytes(value);
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(rw_bit + register);
            inst.data_out[(int)pack.data_0] = temp[0];
            inst.data_out[(int)pack.data_1] = temp[1];
            inst.data_out[(int)pack.data_2] = temp[2];
            inst.data_out[(int)pack.data_3] = temp[3];
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_int;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                //result = BitConverter.ToSingle(inst.payload, pl_offset);
                result = BitConverter.ToInt32(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }

        // Read int register
        public int get_int(int register)
        {
            int result = 0;
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(register);
            inst.data_out[(int)pack.data_0] = 0;
            inst.data_out[(int)pack.data_1] = 0;
            inst.data_out[(int)pack.data_2] = 0;
            inst.data_out[(int)pack.data_3] = 0;
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_float;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                result = BitConverter.ToInt32(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }







        // Write uint register
        public uint set_uint(int register, uint value)
        {
            uint result = 0;
            byte[] temp = BitConverter.GetBytes(value);
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(rw_bit + register);
            inst.data_out[(int)pack.data_0] = temp[0];
            inst.data_out[(int)pack.data_1] = temp[1];
            inst.data_out[(int)pack.data_2] = temp[2];
            inst.data_out[(int)pack.data_3] = temp[3];
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_int;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                //result = BitConverter.ToSingle(inst.payload, pl_offset);
                result = BitConverter.ToUInt32(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }

        // Read uint register
        public uint get_uint(int register)
        {
            uint result = 0;
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(register);
            inst.data_out[(int)pack.data_0] = 0;
            inst.data_out[(int)pack.data_1] = 0;
            inst.data_out[(int)pack.data_2] = 0;
            inst.data_out[(int)pack.data_3] = 0;
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_float;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                result = BitConverter.ToUInt32(inst.payload, pl_offset);
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }






        // Read dual_angles register
        public dual_angles set_dang(int register, int val0, int val1)
        {
            dual_angles result = new dual_angles();
            inst.data_out[(int)pack.header] = (byte)inst.Address;
            inst.data_out[(int)pack.rw_reg] = (byte)(rw_bit + register);
            inst.data_out[(int)pack.data_0] = 0;// (byte)(val0 + 127);
            inst.data_out[(int)pack.data_1] = (byte)(val0 + 127);
            inst.data_out[(int)pack.data_2] = 0;//(byte)(val1 + 127);
            inst.data_out[(int)pack.data_3] = (byte)(val1 + 127);
            inst.data_out[(int)pack.type] = (byte)payload_type.Payload_float;
            inst.data_out[(int)pack.crc] = packet_crc_calc(inst.data_out);
            Transfer(inst.data_out);

            if (inst.state == Transfer_state.OK)
            {
                result.state_0 = inst.payload[pl_offset + 0];
                result.angle_0 = inst.payload[pl_offset + 1];
                result.state_1 = inst.payload[pl_offset + 2];
                result.angle_1 = inst.payload[pl_offset + 3];
                Array.Clear(inst.data_out, 0, inst.payload.Length);
            }
            return result;
        }

    }
}
