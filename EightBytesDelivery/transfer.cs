﻿using System;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EightBytesDelivery;

namespace EBD
{
    public partial class Delivery
    {
        private /*async*/ void Transfer(byte[] buff)
        {
            // Clear
            //Console.WriteLine("\r\ntransfer start");
            //inst.serial.DiscardInBuffer();
            //Array.Clear(inst.data_in, 0, inst.data_in.Length);
            if (inst.serial.IsOpen == false) return;
            inst.serial.Write(buff, 0, buff.Length);

            //await Task.Run(() => Read());
            Read();

            inst.serial.DiscardInBuffer();
            //Console.WriteLine("Write: \t\t" + BitConverter.ToString(inst.data_out));
            //Console.WriteLine("Received:\t" + BitConverter.ToString(inst.data_in));
            //Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
            switch (inst.state)
            {
                case Transfer_state.OK:
                    for(int i = 0; i != 8; i++)
                    {
                        inst.payload[i] = inst.data_in[i];
                    }  
                    break;
                    //Console.WriteLine("receive state: {0}", inst.state.ToString("g"));

                case Transfer_state.crc_error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
                    Console.WriteLine("Write: \t\t" + BitConverter.ToString(inst.data_out));
                    Console.WriteLine("Received:\t" + BitConverter.ToString(inst.data_in));
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case Transfer_state.len_error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
                    Console.WriteLine("Received len = {0}", inst.rec_len.ToString());
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case Transfer_state.exception:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
                    Console.WriteLine("reason: {0}", inst.ex);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case Transfer_state.send:
                    Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
                    break;
                case Transfer_state.wait:
                    Console.WriteLine("receive state: {0}", inst.state.ToString("g"));
                    break;
                default:
                    break;
            }

            //inst.serial.DiscardInBuffer();
            //Array.Clear(inst.data_out, 0, inst.data_out.Length);
        }

        private void Read()
        {
            try
            {
                Thread.Sleep(4);
                inst.rec_len = inst.serial.Read(inst.data_in, 0, 8);

                if (inst.rec_len == 8)
                {
                    byte crc = Crc8.ComputeChecksum(inst.data_in);
                    if (crc == 0x00)
                    {
                        inst.state = Transfer_state.OK;
                    }
                    else
                    {
                        inst.state = Transfer_state.crc_error;
                    }
                }
                else
                {
                    inst.state = Transfer_state.len_error;
                }
            }
            catch(Exception ex)
            {
                inst.ex = ex;
                inst.state = Transfer_state.exception;
            }
        }
    }
}