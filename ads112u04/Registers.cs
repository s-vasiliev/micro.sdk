﻿namespace ads
{
    public partial class ADS1x2U04
    {
        const byte write_offset = 0x40;
        const byte read_offset = 0x20;
        private byte[] ADS_registers = new byte[5];

        // Registers operation
        
        // Get bit field
        private int Get_bit_field(int register, int shift_pos, byte mask)
        {
            byte data = Register_read(register);        // read current register data
            data &= (byte)(mask ^ 0xff);                // inverse mask, clear no data bits
            data = (byte)(data >> shift_pos);           // shift data
            return data;
        }

        // Set bit field
        private void Set_bit_field(int register, int data, int shift_pos, byte mask)
        {
            byte register_current = Register_read(register); // read current register data
            data = data << shift_pos;                        // shift new data
            register_current &= mask;                        // clear bits for new data
            register_current |= (byte)data;                  // set new data bits 
            Register_write(register, register_current);      // write register
        }

        // read register
        private byte Register_read(int register)
        {
            byte result = 0;
            int reg = register;
            if (Serial.IsOpen)
            {
                register = register << 1;
                register |= read_offset;

                byte[] TX = { (byte)Commands.sync, (byte)register };

                Serial.DiscardInBuffer();

                Serial_send_array(TX);
                try
                {
                    result = (byte)Serial.ReadByte();
                }
                catch { }
                ADS_registers[reg] = result;
                return result;
            }
            return 0x00;
        }

        // write register
        private void Register_write(int register, byte data)
        {
            register = register << 1;
            register |= write_offset;
            byte[] TX = { (byte)Commands.sync, (byte)register, data };
            Serial_send_array(TX);
        }

        // Register 0

        #region inputs (MUX)
        /* Input multiplexer configuration.
         * These bits configure the input multiplexer.
         * For settings where AIN N = AVSS, the PGA must be disabled (PGA_BYPASS = 1) 
         * and only gains 1, 2, and 4 can be used */
        public enum AIN
        {
            /*[Description("AINp = AIN0, AINn = AIN1")] */
            AINp_AIN0__AINn_AIN1,
            /*[Description("AINp = AIN0, AINn = AIN2")] */
            AINp_AIN0__AINn_AIN2,
            /*[Description("AINp = AIN0, AINn = AIN3")] */
            AINp_AIN0__AINn_AIN3,
            /*[Description("AINp = AIN1, AINn = AIN0")] */
            AINp_AIN1__AINn_AIN0,
            /*[Description("AINp = AIN1, AINn = AIN2")] */
            AINp_AIN1__AINn_AIN2,
            /*[Description("AINp = AIN1, AINn = AIN3")]*/
            AINp_AIN1__AINn_AIN3,
            /*[Description("AINp = AIN2, AINn = AIN3")] */
            AINp_AIN2__AINn_AIN3,
            /*[Description("AINp = AIN3, AINn = AIN2")] */
            AINp_AIN3__AINn_AIN2,
            /*[Description("AINp = AIN0, AINn = AVSS")] */
            AINp_AIN0__AINn_AVSS,
            /*[Description("AINp = AIN1, AINn = AVSS")]*/
            AINp_AIN1__AINn_AVSS,
            /*[Description("AINp = AIN2, AINn = AVSS")] */
            AINp_AIN2__AINn_AVSS,
            /*[Description("AINp = AIN3, AINn = AVSS")] */
            AINp_AIN3__AINn_AVSS,
            /*[Description("(V(REFp) – V(REFn)) / 4 monitor (PGA bypassed)")]*/
            VREFpVREFn_monitor,
            /*[Description("(AVDD – AVSS) / 4 monitor (PGA bypassed)")]*/
            AVDDAVSS_monitor,
            /*[Description("AINp and AINn shorted to (AVDD + AVSS) / 2")]*/
            AINp_and_AINn_shorted_to_half_AVDD
        }
        public static string[] MUX = {
            "AINp = AIN0, AINn = AIN1",
            "AINp = AIN0, AINn = AIN2",
            "AINp = AIN0, AINn = AIN3",
            "AINp = AIN1, AINn = AIN0",
            "AINp = AIN1, AINn = AIN2",
            "AINp = AIN1, AINn = AIN3",
            "AINp = AIN2, AINn = AIN3",
            "AINp = AIN3, AINn = AIN2",
            "AINp = AIN0, AINn = AVSS",
            "AINp = AIN1, AINn = AVSS",
            "AINp = AIN2, AINn = AVSS",
            "AINp = AIN3, AINn = AVSS",
            "(V(REFp) – V(REFn)) / 4 monitor (PGA bypassed)",
            "(AVDD – AVSS) / 4 monitor (PGA bypassed)",
            "AINp and AINn shorted to (AVDD + AVSS) / 2" };

        public AIN mux
        {
            get
            {
                int reg = 0;
                int shift_pos = 4;
                byte mask = 0b00001111;
                return (AIN)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 0;
                int shift_pos = 4;
                byte mask = 0b00001111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region gain
        public enum Gain
        {
            pga1 = 0,
            pga2 = 1,
            pga4 = 2,
            pga8 = 3,
            pga16 = 4,
            pga32 = 5,
            pga64 = 6,
            pga128 = 7
        }

        // string for GUI
        public static string[] GAIN = new string[] { "1", "2", "4", "8", "16", "32", "64", "128" };

        public Gain Amplifier
        {
            get
            {
                int reg = 0;
                int shift_pos = 1;
                byte mask = 0b11110001;
                return (Gain)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 0;
                int shift_pos = 1;
                byte mask = 0b11110001;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region PGABYPASS
        public enum pga_bypass
        {
            disabled,
            enabled
        }
        public static string[] PGA_BYPASS = new string[] { "enabled", "disabled" };

        public pga_bypass Bypass
        {
            get
            {
                int reg = 0;
                int shift_pos = 0;
                byte mask = 0b11111110;
                return (pga_bypass)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 0;
                int shift_pos = 0;
                byte mask = 0b11111110;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        // Register 1

        #region DR (data rate)
        public enum rate
        {
            sps20or40 = 0,
            sps45or90 = 1,
            sps90or180 = 2,
            sps175or350 = 3,
            sps330or660 = 4,
            sps600or1200 = 5,
            sps1000or2000 = 6
        }
        public static string[] RATE = {
            "    20 SPS / 40 SPS",
            "    45 SPS / 90 SPS",
            "    90 SPS / 180 SPS",
            "  175 SPS / 350 SPS",
            "  330 SPS / 660 SPS",
            "  600 SPS / 1200 SPS",
            "1000 SPS / 2000 SPS", };

        public rate DR
        {
            get
            {
                int reg = 1;
                int shift_pos = 5;
                byte mask = 0b00011111;
                return (rate)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 1;
                int shift_pos = 5;
                byte mask = 0b00011111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region MODE
        public enum mode
        {
            normal = 0,
            turbo = 1
        }
        public static string[] MODE = new string[] { "Normal", "Turbo" };

        public mode OperationMode
        {
            get
            {
                int reg = 1;
                int shift_pos = 4;
                byte mask = 0b11101111;
                return (mode)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 1;
                int shift_pos = 4;
                byte mask = 0b11101111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region CM (Convertion mode)
        public enum conversion
        {
            singleshot,
            continuous
        }
        public static string[] DATACONVERTION = new string[] { "singleshot", "continuous" };

        public conversion ConvertionMode
        {
            get
            {
                int reg = 1;
                int shift_pos = 3;
                byte mask = 0b11110111;
                return (conversion)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 1;
                int shift_pos = 3;
                byte mask = 0b11110111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region referense
        public enum refinput
        {
            internal_ = 0,
            external = 1,
            analog_supply = 2
        }
        public static string[] REFERENCE = new string[] { "internal_", "external", "analog_supply" };

        public refinput Referense
        {
            get
            {
                int reg = 1;
                int shift_pos = 1;
                byte mask = 0b11111001;
                return (refinput)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 1;
                int shift_pos = 1;
                byte mask = 0b11111001;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region TS (Temperature sensor)
        public enum TempSensor
        {
            disables = 0,// spell check failed
            enabled = 1
        }
        public static string[] TEMPSENSOR = new string[] { "Disabled", "Enabled" };

        public TempSensor TS
        {
            get
            {
                int reg = 1;
                int shift_pos = 0;
                byte mask = 0b11111110;
                return (TempSensor)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 1;
                int shift_pos = 0;
                byte mask = 0b11111110;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        // Register 2

        #region drdy
        /* Conversion result ready flag.
        This bit flags if a new conversion result is ready.
        This bit is reset when conversion data are read. */
        public enum drdy
        {
            notready,    // 0 : No new conversion result available
            ready      // 1 : New conversion result ready
        }
        public drdy conv_state
        {
            get
            {
                int reg = 2;
                int shift_pos = 7;
                byte mask = 0b01111111;
                return (drdy)Get_bit_field(reg, shift_pos, mask);
            }
        }
        #endregion

        #region DCNT (Data counter)
        public enum Dcnt
        {
            disabled = 0,
            enabled = 1
        }
        public static string[] DCNT = { "disabled", "enabled" };

        public Dcnt DataCount
        {
            get
            {
                int reg = 2;
                int shift_pos = 6;
                byte mask = 0b10111111;
                return (Dcnt)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 2;
                int shift_pos = 6;
                byte mask = 0b10111111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region CRC
        public enum crc
        {
            disabled = 0,
            inverted = 1,
            crc16 = 2
        }
        public static string[] CRC = { "Disabled", "Inverted data output enabled", "CRC16 enabled", "Reserved" };

        public crc DataCheck
        {
            get
            {
                int reg = 2;
                int shift_pos = 4;
                byte mask = 0b11001111;
                return (crc)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 2;
                int shift_pos = 4;
                byte mask = 0b11001111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region BCS (Burn-out current source)
        public enum Bcs
        {
            disabled,
            enabled
        }
        public static string[] BCS = new string[] { "disabled", "enabled" };

        public Bcs Burnout
        {
            get
            {
                int reg = 2;
                int shift_pos = 3;
                byte mask = 0b11110111;
                return (Bcs)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 2;
                int shift_pos = 3;
                byte mask = 0b11110111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region IDAC
        public static string[] IDAC = { "off", "10 µA", "50 µA", "100 µA", "250 µA", "500 µA", "1000 µA", "1500 µA" };
        public enum idac_current { off, _10_µA, _50µA, _100µA, _250µA, _500µA, _1000µA, _1500µA };

        public idac_current IDACcurrent
        {
            get
            {
                int reg = 2;
                int shift_pos = 0;
                byte mask = 0b11111000;
                return (idac_current)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 2;
                int shift_pos = 0;
                byte mask = 0b11111000;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        // Register 3

        #region I1MUX
        public static string[] IMUX1 =    {
            "IDAC1 disabled",
            "IDAC1 connected to AIN0",
            "IDAC1 connected to AIN1",
            "IDAC1 connected to AIN2",
            "IDAC1 connected to AIN3",
            "IDAC1 connected to REFP",
            "IDAC1 connected to REFN"};

        public enum imux
        {
            disabled = 0,
            idac_ain0 = 1, idac_ain1 = 2, idac_ain2 = 3,
            idac_ain3 = 4, idac_refp = 5, idac_refn = 6
        }

        public imux i1mix
        {
            get
            {
                int reg = 3;
                int shift_pos = 2;
                byte mask = 0b11100011;
                return (imux)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 3;
                int shift_pos = 2;
                byte mask = 0b11100011;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region I2MUX
        public static string[] IMUX2 =    {
            "IDAC2 disabled",
            "IDAC2 connected to AIN0",
            "IDAC2 connected to AIN1",
            "IDAC2 connected to AIN2",
            "IDAC2 connected to AIN3",
            "IDAC2 connected to REFP",
            "IDAC2 connected to REFN"};

        public imux i2mix
        {
            get
            {
                int reg = 3;
                int shift_pos = 5;
                byte mask = 0b00011111;
                return (imux)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 3;
                int shift_pos = 5;
                byte mask = 0b00011111;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        #region AUTO
        public enum Auto
        {
            disable,
            enable
        }
        public static string[] AUTO = { "Single - shot conversion mode", "Continuous conversion mode" };

        public Auto DataOutputMode
        {
            get
            {
                int reg = 3;
                int shift_pos = 0;
                byte mask = 0b11111110;
                return (Auto)Get_bit_field(reg, shift_pos, mask);
            }
            set
            {
                int reg = 3;
                int shift_pos = 0;
                byte mask = 0b11111110;
                Set_bit_field(reg, (int)value, shift_pos, mask);
            }
        }
        #endregion

        // Register 4

        // Устал... не буду пока это делать...
    }
}
