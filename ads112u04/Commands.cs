﻿namespace ads
{
    public partial class ADS1x2U04
    {
        #region ADS_commands
        /*const byte sync = 0x55;
        const byte reset = 0x07;
        const byte startsync = 0x08; // and 0x09
        const byte powerdown = 0x02;
        const byte rdata = 0x10;*/

        enum Commands
        {
            sync = 0x55,
            reset = 0x07,
            startsync = 0x08, // and 0x09
            powerdown = 0x02,
            rdata = 0x10
        }
        #endregion

        // ADS1x2U04 commands {reset, startsync, powerdown, rdata}
        //private void Command(byte command)
        private void Command(Commands command)
        {
            byte[] temp = { (byte)Commands.sync, (byte)command };
            Serial_send_array(temp);
        }
    }
}