﻿
using System.IO.Ports;

namespace ads
{
    public partial class ADS1x2U04
    {
        // SerialPort
        public SerialPort Serial = new SerialPort();
        public SerialPort SerialPort { get { return Serial; } }
        const int ReadTimeout = 42;
        const int WriteTimeout = 40;

        // data recieved event
        private void SerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        // send byte array to serial port
        private void Serial_send_array(byte[] data)
        {
            try
            {
                Serial.Write(data, 0, data.Length);
            }
            catch { }
        }
    }
}