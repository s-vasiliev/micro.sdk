﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.ComponentModel;

namespace ads
{
    public partial class ADS1x2U04
    {
        #region ADS1x2U04 data receive mode
        public enum Mode
        {
            oneshot,        // ADS1x2U04 send convertion data by rdata
            stream          // ADS1x2U04 send convertion data automaticly
            
        }

        public Mode Current_mode = Mode.oneshot; // 
        public bool stream_cycle_is_run; // true - stream cycle is start, false - stream cycle is end
        #endregion

        Thread steram;// = new Thread(update());
        

        private enum Pack // ads112u04, 16 bit, dcnt enable, crc enable.
        {
            dcnt = 0,
            data_l,
            data_h,
            crc_l,
            crc_h
        }

        private enum Pack_24  // 24 bit
        {
            dcnt = 0,
            data_l,
            data_m,
            data_h,
            crc_l,
            crc_h
        }

        public void init()
        {
            if (Serial.IsOpen == false)
            {
                //Serial.PortName = portname;
                //Serial.BaudRate = portbaud;
                Serial.ReadTimeout = ReadTimeout;
                Serial.WriteTimeout = WriteTimeout;
                Serial.Encoding = Encoding.GetEncoding("Windows-1251");
                try
                {
                    Serial.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
                //serial.DataReceived   += new SerialDataReceivedEventHandler(serialPort_DataReceived);
                //port.PinChanged       += new SerialPinChangedEventHandler(port_pin_change);
            }
            Command(Commands.reset);     // reset all register of ADC
            Command(Commands.reset);
            DataCount = Dcnt.enabled;
            DataCheck = crc.crc16;
            //ConvertionMode = conversion.continuous;
            Current_mode = Mode.oneshot;

            //Current_mode = Mode.stream;
            //DataOutputMode = Auto.manual;

            steram = new Thread(delegate () { stream(this); });
            steram.IsBackground = true;
            steram.Start();

            //Serial.DiscardInBuffer();
            //Command(Commands.startsync);
        }
        // constructor
        public ADS1x2U04(string portname, int portbaud, int ADC_bit_width)
        {
            //init();
        }

        public void steram_start()
        {
            DataOutputMode = Auto.enable;
            ConvertionMode = conversion.continuous;
            Serial.DiscardInBuffer();
            Current_mode = Mode.stream;
        }   
        
        // destructor
        ~ADS1x2U04()
        {
            Serial.Dispose();
            Serial.Close();
        }

        // data collector
        public int[] data = new int[256];
        public int datapointer = 0;

        // oneshot data
        private int counter = 0;
        public int Oneshot
        {
            get
            {
                Command(Commands.startsync);
                while (conv_state != drdy.ready ) { counter++; }
                Serial.DiscardInBuffer();
                Command(Commands.rdata);
                counter = 0;
                byte[] raw = new byte[5];
                try
                {
                    raw[(int)Pack.dcnt]     = (byte)Serial.ReadByte(); // dcnt internal counter
                    raw[(int)Pack.data_l]   = (byte)Serial.ReadByte(); // data lsb
                    raw[(int)Pack.data_h]   = (byte)Serial.ReadByte(); // data msb
                    raw[(int)Pack.crc_l]    = (byte)Serial.ReadByte(); // crc0
                    raw[(int)Pack.crc_h]    = (byte)Serial.ReadByte(); // crc1*/
                }
                catch (Exception ex)
                {
                    return 0; /*error_cnt++;*/
                }

                if (check_crc16(raw) != true)  // need some crc magic...

                {
                    return 0;
                }

                datapointer = raw[(int)Pack.dcnt];
                int result = BitConverter.ToInt16(raw, 1);  // <--------- 16 bit version, why about 24 bit?
                data[datapointer] = result;
                return result;                              
            }
        }
        public int cnt = 0;
        // stream data
        private void stream(ADS1x2U04 im)
        {
            while (true)
            {
                switch (Current_mode)
                {
                    case (Mode.oneshot):
                        //DataOutputMode = Auto.disable;
                        stream_cycle_is_run = false;
                        Thread.Sleep(0);
                        break;
                    case (Mode.stream):
                        stream_cycle_is_run = true;
                        byte[] raw = new byte[5];
                        cnt = 0;
                        while (cnt != raw.Length)
                        {
                            try
                            {
                                raw[cnt] = (byte)im.Serial.ReadByte();
                                cnt++;
                            }
                            catch(Exception ex)
                            {
                                Console.WriteLine("counter: {0}, message:\r\n {1}", cnt, ex); // for debug only
                            }
                        }

                        /*try
                        {
                            raw[(int)Pack.dcnt] = (byte)im.Serial.ReadByte(); // dcnt internal counter
                            raw[(int)Pack.data_l] = (byte)im.Serial.ReadByte(); // data lsb
                            raw[(int)Pack.data_h] = (byte)im.Serial.ReadByte(); // data msb
                            raw[(int)Pack.crc_l] = (byte)im.Serial.ReadByte(); // crc0
                            raw[(int)Pack.crc_h] = (byte)im.Serial.ReadByte(); // crc1
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);// for debug only
                            Serial.DiscardInBuffer();
                        }*/

                        if (im.check_crc16(raw) == true)  // need some crc magic...
                        {
                            im.datapointer = raw[(int)Pack.dcnt];
                            int result = BitConverter.ToInt16(raw, 1);
                            im.data[im.datapointer] = result;
                        }
                        else
                        {
                            Serial.DiscardInBuffer();
                        }
                        break;
                    default:
                        break;
                }
                stream_cycle_is_run = false;
                Thread.Sleep(0);
            }
        }





        // Math

        // calculate ADS122U04 (24-bit) conversion result <<------------------------!!! DO NOT USE !!!
        private int calculate24(byte[] buff)
        {
            int result = 0;
            //if (buff.Length < 3) { return 0; }
            //int result = buff[1] + buff[2] * 256;
            //if (result >= 32768) { result = result - 65536; }
            return result;
        }

        // Check ADS1x2U04 answer with inverted bits
        private bool check_inverted(byte[] data)
        {
            if (data.Length != 4) { return false; }
            bool first = data[0] == (255 - data[2]);
            bool second = data[1] == (255 - data[3]);
            bool result = first && second;
            return result;
        }

        // Check ADS1x2U04 answer with Crc16Ccitt
        private bool check_crc16(byte[] data)
        {
            bool result = true;
            if (data.Length != 5) { return false; }
            byte[] crcBuffer = Crc16Ccitt.ComputeChecksumBytes(
                data[(int)Pack.dcnt], data[(int)Pack.data_l], data[(int)Pack.data_h]);
            if (data[(int)Pack.crc_l] != crcBuffer[0]) { result = false; }
            if (data[(int)Pack.crc_h] != crcBuffer[1]) { result = false; }
            return result;
        }
    }
}

// OLD version !!! Deprecated !!! DO NOT USE !!!
namespace ads112u04
{

    public class thread_control
    {
        public static void stop_all()
        {
            try
            {
                recorder.threadRecorder.Abort();
                Service.threadService.Abort();
                Thread.Sleep(10);
            }
            catch { }
        }
    }

    public class math  // 
    {
        // return average of input buffer
        public static int average(int[] buff)
        {
            int result = 0;
            for (int n = 0; n != buff.Length; n++)
            {
                result += buff[n];
            }
            result = result / buff.Length;
            return result;
        }
    }

    public class voltage
    {
        public static double sample_cost = 0.0000625;
        public static double[] cost = new double[8] {
            0.0000625,                  // pga = 1, reference 2.048V
            0.00003125,                 // pga = 2, reference 2.048V
            0.000015625,                // pga = 4, reference 2.048V
            0.0000078125,               // pga = 8, reference 2.048V
            0.00000390625,              // pga = 16, reference 2.048V
            0.000001953125,             // pga = 32, reference 2.048V
            0.0000009765625,            // pga = 64, reference 2.048V
            0.00000048828125            // pga = 128, reference 2.048V
        };
    }

    public class connect
    {
        const byte sync = 0x55;
        const byte write_offset = 0x40;
        const byte read_offset = 0x20;

        public static SerialPort _serial;

        public static bool check()
        {
            return _serial.IsOpen;
        }

        public static bool open(string portname, int portbaud)
        {
            SerialPort port = _serial;
            if (port.IsOpen == false)
            {
                port.PortName = portname;
                port.BaudRate = portbaud;
                port.ReadTimeout = 80;
                port.WriteTimeout = 40;
                port.Encoding = Encoding.GetEncoding("Windows-1251");
                try
                {
                    port.Open();
                } catch { return false; }
                //port.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
                //port.PinChanged += new SerialPinChangedEventHandler(port_pin_change);
            }
            return port.IsOpen;
        }

        private static void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public static byte readregister(int register)
        {
            if (_serial == null) return 0;

            SerialPort port = _serial;
            byte result = 0;
            if (port.IsOpen)
            {
                port.DiscardInBuffer();
                register = register << 1;
                register |= read_offset;

                byte[] TX = { sync, (byte)register };
                send_data(TX);
                try
                {
                    result = (byte)port.ReadByte();
                }
                catch { }
                return result;
            }
            return 0;
        }

        //public static byte writeregister(int register, byte data)
        public static void writeregister(int register, byte data)
        {
            register = register << 1;
            register |= write_offset;
            byte[] TX = { sync, (byte)register, data };
            send_data(TX);
            //return readregister(register);
        }

        public static void send_data(byte[] data)
        {
            SerialPort port = _serial;
            if (!port.IsOpen)
            {
                return;
            }
            try
            {
                port.Write(data, 0, data.Length);
            }
            catch { }
        }
    }

    public class register
    {
        public static void set_field(int register, int data, int shift_pos, byte mask)
        {
            //int reg = ;
            //int shift_pos = ;
            //byte mask = ;
            //register.set_field(reg, (int)xxx, shift_pos, mask);

            byte register_current = connect.readregister(register); // read current register data
            data = data << shift_pos;                               // shift new data
            register_current &= mask;                               // clear bits for new data
            register_current |= (byte)data;                         // set new data bits 
            connect.writeregister(register, register_current);      // write register
        }

        public static byte get_field(int register, int shift_pos, byte mask)
        {
            byte result = 0x00;
            // not yet
            return result;
        }
    }

    public class recorder
    {
        const int array_size = 0x4000000; // 64Mb
        public static Thread threadRecorder;
        
        public static byte[] rawdata = new byte[array_size];
        public static int rawcounter = 0;
        public static int errorcounter = 0;
        public static int[] samples = new int[array_size];
        //public static byte[] samples_internal_counter = new byte[array_size];
        public static int sample_counter = 0;
        private static int msb = 0;

        public static void init()
        {
            msb = 0;
            sample_counter = 0;
            rawcounter = 0;
            errorcounter = 0;
            threadRecorder = new Thread(Recorder);
            threadRecorder.IsBackground = true;
        }

        static public void Recorder()
        {
            SerialPort port = connect._serial;
            port.DiscardInBuffer();
            while (rawcounter != rawdata.Length)
            {
                try
                {
                    rawdata[rawcounter++] = (byte)port.ReadByte();
                }
                catch { errorcounter++; }
                
                if (rawcounter == rawdata.Length)
                {
                    rawcounter--;
                    DoStop();
                }
                rawparser();
            }
        }

        private static void rawparser()
        {
            if (rawcounter > 5)
            {
                byte[] buff = {
                        rawdata[rawcounter-5],
                        rawdata[rawcounter-4],
                        rawdata[rawcounter-3],
                        rawdata[rawcounter-2],
                        rawdata[rawcounter-1]
                    };

                bool check = datacheck.check_crc16(buff);
                if (check)
                {
                    int temp = get.calculate(buff);  // get sample of raw data
                    sample_counter = msb * 256 + buff[0];  //buff[0] - internal adc counter
                    samples[sample_counter] = temp;
                    if (buff[0] == 255) { msb++; }
                }
            }
        }

        static public void DoStart()
        {
            thread_control.stop_all();
            command.startsync();
            threadRecorder.Start();
        }

        static public void DoStop()
        {
            //Thread threadReceiver = recorder.threadRecorder;
            command.powerdown();
            threadRecorder.Abort();
            System.Threading.Thread.Sleep(1);
            command.powerdown();
            //counter = 0;
        }
    }

    public class get
    {
        public static byte[] sample_raw = new byte[5];
        public static int counter = 0;
        public static int _sample = 0;
        public static int error_cnt = 0;

        public static void init()
        {
            command.reset();
            Thread.Sleep(2);
            datacheck.dcnt(datacheck.counter.enabled);
            datacheck.use(datacheck.crc.crc16);
            //datarate.operating_mode(datarate.mode.normal);
            //datarate.dr(datarate.rate.sps20or40);
            //dataconversion.use(dataconversion.conversion.singleshot);
            //datasend.use(datasend.mode.manual);
            error_cnt = 0;
        }

        // dcnt, crc16, convertion manual, datasend manual, 20sps
        public static int sample()
        {
            SerialPort port = connect._serial;

            command.startsync();            // start convertion
            
            // get five bytes of raw data
            if (port.IsOpen)
            {
                port.DiscardInBuffer();
                command.rdata();
                try
                {
                    sample_raw[0] = (byte)port.ReadByte(); // dcnt internal counter
                    sample_raw[1] = (byte)port.ReadByte(); // data lsb
                    sample_raw[2] = (byte)port.ReadByte(); // data msb
                    sample_raw[3] = (byte)port.ReadByte(); // crc0
                    sample_raw[4] = (byte)port.ReadByte(); // crc1
                }
                catch { error_cnt++; }
            } 
            else { return 0; }

            // check crc
            // calculate sample
            if (datacheck.check_crc16(sample_raw))
            {
                _sample = calculate(sample_raw);           
            }
            return _sample;
        }

        // 16-bit only
        public static int calculate(byte[] buff)
        {
            //if (buff.Length < 3) { return 0; }
            int result = buff[1] + buff[2] * 256;
            if (result >= 32768) { result = result - 65536; }
            return result;
        }
    }

    public static class Service
    {
        public static Thread threadService;
        public static double temperature_ = 0.0;
        public static double avdd_ = 0.0;
        public static double ext_ref_ = 0.0;

        public static void service()
        {
            while (true)
            {
                temperature_ = temperature();
                avdd_ = avdd();
                ext_ref_ = ext_ref();
                Thread.Sleep(100);
            }
        }

        public static void init()
        {
            threadService = new Thread(service);
        }

        static public void DoStart()
        {
            thread_control.stop_all();
            ads112u04.Service.init();
            threadService.Start();
        }

        static public void DoStop()
        {
            try
            {
                threadService.Abort();
            }
            catch { }
        }

        public static double temperature()
        {
            get.init();
            ads112u04.temperature.sensor(ads112u04.temperature.mode.enabled);
            //datarate.dr(datarate.rate.sps20or40);
            command.startsync();
            const double lsb_cost = 0.03125;
            System.Threading.Thread.Sleep(200);
            int temp = get.sample();
            temp = temp >> 2;
            return temp * lsb_cost;
        }

        public static double avdd()
        {
            get.init();
            //datarate.dr(datarate.rate.sps20or40);
            mux.use(mux.inputs.AVDDAVSS_monitor);
            command.startsync();
            const double lsb_cost = 0.0000625;
            System.Threading.Thread.Sleep(200);
            int temp = get.sample();
            return temp * lsb_cost * 4;
        }

        public static double ext_ref()
        {
            get.init();
            //datarate.dr(datarate.rate.sps20or40);
            mux.use(mux.inputs.VREFpVREFn_monitor);
            command.startsync();
            const double lsb_cost = 0.0000625;
            System.Threading.Thread.Sleep(200);
            int temp = get.sample();
            return temp * lsb_cost * 4;
        }
    }

    public static class temperature
    {
        public enum mode
        {
            disabled,
            enabled
        }
        public static string[] MODE = { "disabled", "enabled" };
        public static void sensor(temperature.mode m)
        {
            int reg = 1;
            int shift_pos = 0;
            byte mask = 0b11111110;
            register.set_field(reg, (int)m, shift_pos, mask);
        }
    }

    public static class command
    {
        const byte sync             = 0x55;
        const byte byte_reset       = 0x07;
        const byte byte_startsync   = 0x08; // and 0x09
        const byte byte_powerdown   = 0x02;
        const byte byte_rdata       = 0x10;

        public static void reset()
        {
            byte[] temp = { sync, byte_reset };
            connect.send_data(temp);
        }
        public static void startsync()
        {
            byte[] temp = { sync, byte_startsync };
            connect.send_data(temp);
        }
        public static void powerdown()
        {
            byte[] temp = { sync, byte_powerdown };
            connect.send_data(temp);
        }
        public static void rdata()
        {
            byte[] temp = { sync, byte_rdata };
            connect.send_data(temp);
        }
    }

    public static class datacheck
    {
        public enum crc
        {
            disabled = 0,
            inverted = 1,
            crc16    = 2
        }

        public static string[] CRC = { "Disabled", "Inverted data output enabled", "CRC16 enabled", "Reserved" };
        public static void use(crc check)
        {
            int reg = 2;
            int shift_pos = 4;
            byte mask = 0b11001111;
            register.set_field(reg, (int)check, shift_pos, mask);
        }

        public enum counter
        {
            disabled = 0,
            enabled = 1
        }
        public static string[] DCNT = { "disabled", "enabled" };

        public static void dcnt(counter d)
        {
            int reg = 2;
            int shift_pos = 6;
            byte mask = 0b10111111;
            register.set_field(reg, (int)d, shift_pos, mask);
        }

        public static bool check_inverted(byte[] data)
        {
            if (data.Length != 4) { return false; }
            bool first = data[0] == (255 - data[2]);
            bool second = data[1] == (255 - data[3]);
            bool result = first && second;
            return result;
        }

        public static bool check_crc16(byte[] data)
        {
            bool result = true;
            if (data.Length != 5) { return false; }
            byte[] crcBuffer = Crc16Ccitt.ComputeChecksumBytes(data[0], data[1], data[2]);
            if (data[3] != crcBuffer[0]) { result = false; }
            if (data[4] != crcBuffer[1]) { result = false; }
            return result;
        }
    }

    public static class datarate
    {
        public enum rate
        {
            sps20or40 = 0,
            sps45or90 = 1,
            sps90or180 = 2,
            sps175or350 = 3,
            sps330or660 = 4,
            sps600or1200 = 5,
            sps1000or2000 = 6
        }
        public static string[] RATE = {
            "    20 SPS / 40 SPS",
            "    45 SPS / 90 SPS",
            "    90 SPS / 180 SPS",
            "  175 SPS / 350 SPS",
            "  330 SPS / 660 SPS",
            "  600 SPS / 1200 SPS",
            "1000 SPS / 2000 SPS", };

        public static void dr(rate dr)
        {
            int reg = 1;
            int shift_pos = 5;
            byte mask = 0b00011111;
            register.set_field(reg, (int)dr, shift_pos, mask);

            /*byte register1 = connect.readregister(1);
            int value = (int)dr;
            value = value << 5;
            register1 &= 0b00011111;    // clear bits  dr
            register1 |= (byte)value;   // set bits dr
            connect.writeregister(1, register1);*/
        }

        public enum mode
        {
            normal = 0,
            turbo = 1
        }

        public static void operating_mode(mode m)
        {
            int reg = 1;
            int shift_pos = 4;
            byte mask = 0b11101111;
            register.set_field(reg, (int)m, shift_pos, mask);
            
            /*byte register1 = connect.readregister(1);
            int value = (int)m;
            value = value << 4;
            register1 &= 0b11101111;    // clear bits  mode
            register1 |= (byte)value;   // set bits mode
            connect.writeregister(1, register1);*/
        }
    }

    public static class idac
    {
        public static string[] IMUX1 =    {
            "IDAC1 disabled",
            "IDAC1 connected to AIN0",
            "IDAC1 connected to AIN1",
            "IDAC1 connected to AIN2",
            "IDAC1 connected to AIN3",
            "IDAC1 connected to REFP",
            "IDAC1 connected to REFN"};

        public static string[] IMUX2 =    {
            "IDAC2 disabled",
            "IDAC2 connected to AIN0",
            "IDAC2 connected to AIN1",
            "IDAC2 connected to AIN2",
            "IDAC2 connected to AIN3",
            "IDAC2 connected to REFP",
            "IDAC2 connected to REFN"};
        public enum imux
        {
            disabled = 0,
            idac_ain0 = 1, idac_ain1 = 2, idac_ain2 = 3,
            idac_ain3 = 4, idac_refp = 5, idac_refn = 6
        }

        public static void i1mix(imux m)
        {
            int reg = 3;
            int shift_pos = 2;
            byte mask = 0b11100011;
            register.set_field(reg, (int)m, shift_pos, mask);
        }

        public static void i2mix(imux m)
        {
            int reg = 3;
            int shift_pos = 5;
            byte mask = 0b00011111;
            register.set_field(reg, (int)m, shift_pos, mask);

        }

        public static string[] IDAC = { "off", "10 µA", "50 µA", "100 µA", "250 µA", "500 µA", "1000 µA", "1500 µA" };
        public enum idac_current      {  off,  _10_µA,  _50µA,   _100µA,   _250µA,   _500µA,   _1000µA,   _1500µA }

        public static void idac_set(idac_current idac)
        {
            int reg = 2;
            int shift_pos = 0;
            byte mask = 0b11111000;
            register.set_field(reg, (int)idac, shift_pos, mask);
        }
    }

    public static class referense
    {
        public enum refinput
        {
            internal_ = 0,
            external = 1,
            analog_supply = 2
        }
        public static string[] REFERENCE = new string[] { "internal_", "external", "analog_supply" };

        public static void use(refinput r)
        {
            int reg = 1;
            int shift_pos = 1;
            byte mask = 0b11111001;
            register.set_field(reg, (int)r, shift_pos, mask);
        }
    }

    public static class gpio
    {
        public enum direction { input, output }
        public enum gpio2sel { gpio2dat, drdy}
        public enum pin { gpio0, gpio1, gpio2 }
        public enum level { low, high}

        public static string[] GPIO0DIR = { "Input", "Output" };
        public static string[] GPIO0DAT = { "Logic low", "Logic high" };
        public static string[] GPIO1DIR = { "Input", "Output" };
        public static string[] GPIO1DAT = { "Logic low", "Logic high" };
        public static string[] GPIO2DIR = { "Input", "Output" };
        public static string[] GPIO2DAT = { "Logic low", "Logic high" };
        public static string[] GPIO2SEL = { "GPIO2DAT", "DRDY" };

        public static void set_direction(pin p, direction d)
        {
            int reg = 4;
            int shift_pos;
            byte mask;
            switch ((int)p)
            {
                case 0:
                    shift_pos = 4;
                    mask = 0b11101111;
                    break;
                case 1:
                    shift_pos = 5;
                    mask = 0b11011111;
                    break;
                case 2:
                    shift_pos = 6;
                    mask = 0b10111111;
                    break;
                default:
                    shift_pos = 0;
                    mask = 0b11111111;
                    break;
            }
            register.set_field(reg, (int)d, shift_pos, mask);
        }

        public static void gpio2drdy(gpio2sel sel)
        {
            int reg = 4;
            int shift_pos = 3;
            byte mask = 0b11110111;
            register.set_field(reg, (int)sel, shift_pos, mask);
        }

        public static void set_pin(pin p, level s)
        {
            int reg = 4;
            int shift_pos;
            byte mask;
            switch ((int)p)
            {
                case 0:
                    shift_pos = 0;
                    mask = 0b11111110;
                    break;
                case 1:
                    shift_pos = 1;
                    mask = 0b11111101;
                    break;
                case 2:
                    shift_pos = 2;
                    mask = 0b11111011;
                    break;
                default:
                    shift_pos = 0;
                    mask = 0b11111111;
                    break;
            }
            register.set_field(reg, (int)s, shift_pos, mask);
        }

        public static bool get_pin(pin p)
        {
            // not yet
            return false;
        }
    }

    public static class drdy { }

    public static class datasend
    {
        public enum mode
        {
            manual,
            automatic
        }
        public static string[] DATASEND = { "Single - shot conversion mode", "Continuous conversion mode" };

        public static void use(mode m)
        {
            int reg = 3;
            int shift_pos = 0;
            byte mask = 0b11111110;
            register.set_field(reg, (int)m, shift_pos, mask);
        }
    }
    
    public static class dataconversion
    {
        public enum conversion
        {
            singleshot,
            continuous
        }
        public static string[] DATACONVERTION = new string[] { "singleshot", "continuous" };
        public static void use(conversion cm)
           {
            int reg = 1;
            int shift_pos = 3;
            byte mask = 0b11110111;
            register.set_field(reg, (int)cm, shift_pos, mask);
        }

    }
    
    public static class bcs
    {
        public enum mode
        {
            disabled,
            enabled
        }
        public static string[] BCS = new string[] { "disabled", "enabled" };
        public static void state(bcs.mode m)
        {
            int reg = 2;
            int shift_pos = 3;
            byte mask = 0b11110111;
            register.set_field(reg, (int)m, shift_pos, mask);
        }
    }

    public class mux
    {
        public enum inputs
        {
            AINp_AIN0__AINn_AIN1,
            AINp_AIN0__AINn_AIN2,
            AINp_AIN0__AINn_AIN3,
            AINp_AIN1__AINn_AIN0,
            AINp_AIN1__AINn_AIN2,
            AINp_AIN1__AINn_AIN3,
            AINp_AIN2__AINn_AIN3,
            AINp_AIN3__AINn_AIN2,
            AINp_AIN0__AINn_AVSS,
            AINp_AIN1__AINn_AVSS,
            AINp_AIN2__AINn_AVSS,
            AINp_AIN3__AINn_AVSS,
            VREFpVREFn_monitor,
            AVDDAVSS_monitor,
            AINp_and_AINn_shorted_to_half_AVDD
        }
        public static string[] MUX = {
            "AINp = AIN0, AINn = AIN1",
            "AINp = AIN0, AINn = AIN2",
            "AINp = AIN0, AINn = AIN3",
            "AINp = AIN1, AINn = AIN0",
            "AINp = AIN1, AINn = AIN2",
            "AINp = AIN1, AINn = AIN3",
            "AINp = AIN2, AINn = AIN3",
            "AINp = AIN3, AINn = AIN2",
            "AINp = AIN0, AINn = AVSS",
            "AINp = AIN1, AINn = AVSS",
            "AINp = AIN2, AINn = AVSS",
            "AINp = AIN3, AINn = AVSS",
            "(V(REFp) – V(REFn)) / 4 monitor (PGA bypassed)",
            "(AVDD – AVSS) / 4 monitor (PGA bypassed)",
            "AINp and AINn shorted to (AVDD + AVSS) / 2" };

        public static void use(inputs value)
        {
            int reg = 0;
            int shift_pos = 4;
            byte mask = 0b00001111;
            register.set_field(reg, (int)value, shift_pos, mask);
        }
    }

    public class amplifier
    {
        public enum gain
        {
            pga1 = 0,
            pga2 = 1,
            pga4 = 2,
            pga8 = 3,
            pga16 = 4,
            pga32 = 5,
            pga64 = 6,
            pga128 = 7
        }

        public static string[] GAIN = new string[] { "1", "2", "4", "8", "16", "32", "64", "128" };

        public static void set(gain value)
        {
            /*byte register0 = connect.readregister(0);
            int val = (int)value << 1;
            register0 &= 0b11110001;    // clear bits gain
            register0 |= (byte)val;     // set bits gain
            connect.writeregister(0, register0);*/

            int reg = 0;
            int shift_pos = 1;
            byte mask = 0b11110001;
            register.set_field(reg, (int)value, shift_pos, mask);
        }

        public enum pga_bypass
        {
            disabled,
            enabled
        }
        public static string[] PGA_BYPASS = new string[] { "enabled", "disabled" };

        public static void pga_bypass_mode(pga_bypass value)
        {
            int reg = 0;
            int shift_pos = 0;
            byte mask = 0b11111110;
            register.set_field(reg, (int)value, shift_pos, mask);
        }
    }

}
